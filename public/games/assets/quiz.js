var quiz = {
    // (A) PROPERTIES
    // (A1) QUESTIONS & ANSWERS
    // Q = QUESTION, O = OPTIONS, A = CORRECT ANSWER
    data: [{
            q: "Người Thái với tên tự gọi là gì?(tuỳ thuộc vào khu vực)",
            o: ["Tây", "Tái", "Tãy", "Thây"],
            a: 1, // arrays start with 0, so answer is 70 meters
        },
        {
            q: "Các nhóm ngành lớn của người Thái tại Việt Nam",
            o: [
                "Tay Đón, Tay Đăm, Tay Đèng, Tay Dọ",
                "Thái Trắng, Thái Đen, Thái Đỏ, Thái Yo",
                "Tay Đón, Tay Đăm",
                "Cả ba đáp án trên",
            ],
            a: 3,
        },
        {
            q: "Người Thái tập trung chủ yếu ở đâu?",
            o: ["Nghệ An", "Thanh Hoá", "Sơn La", "Lai Châu"],
            a: 2,
        },
        {
            q: "Nền kinh tế chủ yếu của người dân tộc Thái là gì?",
            o: [
                "Lúa nước",
                "Gia súc, gia cầm",
                "Dệt vải, đồ gốm",
                "Tất cả các đáp án trên",
            ],
            a: 3,
        },
        {
            q: "Điểm khác biệt nhất của nhà cửa người Thái so với người Việt và Hán là?",
            o: [
                "Họ ở trên cây",
                "Họ ở nhà sàn",
                "Họ ở trong hang động",
                "Họ ở dưới hầm",
            ],
            a: 1,
        },
    ],

    // (A2) HTML ELEMENTS
    hWrap: null, // HTML quiz container
    hQn: null, // HTML question wrapper
    hAns: null, // HTML answers wrapper
    // hNext: null,
    // (A3) GAME FLAGS
    now: 0, // current question
    score: 0, // current score
    wrongSound: new Audio("/games/assets/wrong.mp3"),
    correctSound: new Audio("/games/assets/correct.mp3"),
    winSound: new Audio("/games/assets/win.mp3"),
    loseSound: new Audio("/games/assets/lose.mp3"),

    // (B) INIT QUIZ HTML
    init: () => {
        // (B1) WRAPPER
        quiz.hWrap = document.getElementById("quizWrap");

        // (B2) QUESTIONS SECTION
        quiz.hQn = document.createElement("div");
        quiz.hQn.id = "quizQn";
        quiz.hWrap.appendChild(quiz.hQn);

        // (B3) ANSWERS SECTION
        quiz.hAns = document.createElement("div");
        quiz.hAns.id = "quizAns";
        quiz.hWrap.appendChild(quiz.hAns);

        // quiz.hNext = document.createElement("button");
        // quiz.hNext.id = "quizNext";
        // quiz.hNext.class = "text-right hNext";
        // quiz.hNext.innerHTML = "Next";
        // quiz.hWrap.appendChild(quiz.hNext);

        // (B4) GO!
        quiz.draw();
    },

    // (C) DRAW QUESTION
    draw: () => {
        // (C1) QUESTION
        quiz.hQn.innerHTML = quiz.data[quiz.now].q;

        // (C2) OPTIONS
        quiz.hAns.innerHTML = "";
        for (let i in quiz.data[quiz.now].o) {
            let radio = document.createElement("input");
            radio.type = "radio";
            radio.name = "quiz";
            radio.id = "quizo" + i;
            quiz.hAns.appendChild(radio);
            let label = document.createElement("label");
            label.innerHTML = quiz.data[quiz.now].o[i];
            label.setAttribute("for", "quizo" + i);
            label.dataset.idx = i;
            label.addEventListener("click", () => {
                quiz.select(label);
            });
            quiz.hAns.appendChild(label);
        }
    },

    // (D) OPTION SELECTED
    select: (option, next = false, delay = 3000) => {
        // (D1) DETACH ALL ONCLICK
        let all = quiz.hAns.getElementsByTagName("label");
        for (let label of all) {
            label.removeEventListener("click", quiz.select);
        }

        // (D2) CHECK IF CORRECT
        let correct = option.dataset.idx == quiz.data[quiz.now].a;
        let description = document.createElement("div");
        let question = document.getElementById("quizAns");
        if (!next) {
            if (correct) {
                quiz.correctSound.play();
                description.innerHTML = "Chính xác!";
                quiz.score++;
                option.classList.add("correct");
                question.appendChild(description);
            } else {
                quiz.wrongSound.play();
                let answer = quiz.data[quiz.now].a;
                description.innerHTML =
                    "Chưa chính xác.Rất tiếc!! Đáp án đúng là đáp án: " +
                    quiz.data[quiz.now].o[answer];
                option.classList.add("wrong");
                question.appendChild(description);
            }
        }

        // (D3) NEXT QUESTION OR END GAME
        quiz.now++;
        setTimeout(() => {
            if (quiz.now < quiz.data.length) {
                quiz.draw();
            } else {
                quiz.hQn.innerHTML = `Bạn đã trả lời ${quiz.score} of ${quiz.data.length} correctly.`;
                quiz.hAns.innerHTML = "";
                if (quiz.score > 3) quiz.winSound.play();
                else quiz.loseSound.play();
                let confirm = document.createElement("button");
                confirm.innerHTML = "Chơi lại";
                confirm.setAttribute("value", "continue");
                confirm.id = "confirmBtn"
                confirm.className = "myButton redButton";
                confirm.addEventListener("click", () => {
                    quiz.reset();
                });
                let back = document.createElement("button");
                back.setAttribute("value", "back");
                back.className = "myButton darkButton";
                back.id = "backBtn";
                let linkBack = document.createElement("a");
                linkBack.setAttribute("href", "/all-games");
                linkBack.innerHTML = "Quay về";
                back.appendChild(linkBack);
                // document.getElementById("quizNext").remove();
                quiz.hWrap.appendChild(confirm);
                quiz.hWrap.appendChild(back);
            }
        }, delay);
    },

    // (E) RESTART QUIZ
    reset: () => {
        quiz.now = 0;
        quiz.score = 0;
        quiz.draw();
        let cfBtn = document.getElementById("confirmBtn");
        let backBtn = document.getElementById("backBtn");
        cfBtn.remove();
        backBtn.remove();
    },
};
window.addEventListener("load", quiz.init);
