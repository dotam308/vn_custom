@extends('mainLayout')
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="main-form">
                    <h3>Chỉnh sửa</h3>
                    <br> <br>
                    <form action="{{ route('editDetailCustom') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <table class="table table-striped">
                            <tbody id="add_more_images">
                                <input type="hidden" name="id" value="{{ $custom->id }}" />
                                <tr class="row">
                                    <th class="col-md">Code</th>
                                    <td class="col-md">{{ $custom->id }}</td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Tiêu đề</th>
                                    <td class="col-md"><input name="title" type="text" class="form-control"
                                            value="{{ $custom->title }}" /></td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Tóm tắt</th>
                                    <td class="col-md"><textarea name="short_description" type="text"
                                            class="form-control">{{ $custom->short_description }} </textarea></td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Nội dung chính</th>
                                    <td class="col-md" style="height: 200px"><textarea name="content" type="text"
                                            class="form-control" style="height: 200px">{{ $custom->content }}</textarea>
                                    </td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Ảnh hiển thị
                                        @if ($custom->displayed_image_id)
                                            <button class="btn btn-danger row-{{ $custom->displayed_image_id }}"
                                                type="button" id="remove-{{ $custom->displayed_image_id }}"
                                                data-id="{{ $custom->displayed_image_id }}">Xóa
                                            </button>
                                        @endif
                                    </th>
                                    <td class="col-md">
                                        <img src="{{ $custom->displayedImage ? $custom->displayedImage->directory : '' }}"
                                            style="width: 30%" class="row-{{ $custom->displayed_image_id ?? '' }}" />
                                        <input type="file" name="image" class="form-control">
                                    </td>
                                </tr>

                                <hr>
                                @forelse ($custom->images as  $image)
                                    <tr class="row {{ 'row-' . $image->id }}">
                                        <th class="col-md">Ảnh nội dung
                                            <button class="btn btn-danger" type="button" id="remove-{{ $image->id }}"
                                                data-id="{{ $image->id }}">Xóa
                                            </button>
                                        </th>
                                        <td class="col-md">
                                            <img src="{{ $image->directory }}" />
                                            <input type="file" name="oldImages[]" class="form-control">
                                        </td>
                                    </tr>
                                    <tr class="row {{ 'row-' . $image->id }}">
                                        <th class="col-md">Caption</th>
                                        <td class="col-md">
                                            <input type="text" name="oldTitles[]" class="form-control"
                                                value="{{ $image->title }}">
                                        </td>
                                    </tr>
                                    <tr class="row {{ 'row-' . $image->id }}">
                                        <th class="col-md">Content</th>
                                        <td class="col-md">
                                            <textarea type="text" name="oldContents[]" class="form-control"
                                                style="height: 200px">
                                                                                                    {{ $image->content }}
                                                                                                </textarea>
                                        </td>
                                    </tr>
                                @empty

                                @endforelse
                            </tbody>

                            <tr class="row">
                                <th class="col-md">
                                    <button class="btn btn-warning" type="button" id="editImage">Thêm</button>
                                </th>
                                <td class="col-md">

                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-primary">Xác nhận</button>
                        <button class="btn btn-info"><a href="{{ route('manageCustoms') }}">Quay lại</a></button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#editImage").click(function() {
                let image = " <tr class='row'> " +
                    "<th class='col-md'>Ảnh nội dung</th> " +
                    "<td class='col-md'>" +
                    "<input type='file' name='images[]' class='form-control'>" +
                    " </td></tr>" +
                    " <tr class='row'> " +
                    "<th class='col-md'>Caption</th> " +
                    "<td class='col-md'>" +
                    "<input type='text' name='titles[]' class='form-control'>" +
                    " </td></tr>" +
                    " <tr class='row'> " +
                    "<th class='col-md'>Content</th> " +
                    "<td class='col-md'>" +
                    "<input type='text' name='contents[]' class='form-control'>" +
                    " </td></tr>";
                $("#add_more_images").append(image);
            });

            $("button[id^='remove']").click(function() {
                let key = $(this).attr("data-id");
                let row = ".row-" + key;
                if (key && key != "") {
                    let confirmation = confirm(
                        "Bạn chắc chắn chứ? Hành động này sẽ không được khôi phục đâu nhé!");
                    if (confirmation) {
                        $(row).remove();

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "/delete-custom-image/" + key,
                            type: "DELETE",
                            contentType: "application/json",
                            success: function() {
                                alert("Hình ảnh được xóa thành công.");
                            },
                            error: function(data, textStatus, errorThrown) {
                                console.log(data);

                            },
                        });
                    }
                }


            });

        });
    </script>
@endsection
