@extends('mainLayout')
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->


    <section id="gallery" class="packages">
        <div class="container">
            <div class="gallary-header text-center">
                <h2>
                    {{ $custom->title }}
                </h2>
            </div>
            <div>
                <p class="timesNew">
                    {{ $custom->content }}
                </p>
                @forelse ($custom->images as $image)
                    <div class="text-center">
                        <img src="{{ $image->directory }}" alt="hinh anh" />
                    </div>
                    <figcaption class="text-center timesNew">{{ $image->title }}</figcaption>
                    <p class="timesNew">{{ $image->content }}</p>
                    <br>
                    <br>
                @empty

                @endforelse
            </div>

            <div class="row">
                <div class="about-btn col-sm">
                    <a class="btn about-view packages-btn" href="{{ route('allCustoms') }}" style="color: white">
                        Quay lại
                    </a>
                </div>
            </div>
            <section id="comment"></section>
            <div class="fb-comments" data-href="{{ env('APP_URL') }}detail-custom/{{ $custom->id }}#comment"
                data-width="1000" data-numposts="5"></div>
            <br><br><br>
            <h2>Có thể bạn quan tâm</h2>
            <div class="gallery-box">
                <div class="gallery-content">
                    <div class="filtr-container">
                        <div class="row">
                            @foreach ($relatedCustoms as $custom)
                                <div class="col-md-6">
                                    @if (session('role') && session('role') == 'admin')
                                        <a class="fa fa-edit"
                                            href="{{ route('editCustom', ['id' => $custom->id]) }}"></a>
                                        <a class="fa fa-trash" style="color: red"
                                            href="{{ route('deleteCustom', ['id' => $custom->id]) }}"></a>
                                    @endif
                                    <div class="filtr-item">

                                        <img src="{{ $custom->displayedImage ? $custom->displayedImage->directory : '\images\xoi_ngu_sac.jpg' }}"
                                            alt="portfolio image" />
                                        <div class="item-title">
                                            <a href="{{ route('detailCustom', $custom->id) }}">
                                                {{ $custom->title }}
                                            </a>
                                        </div><!-- /.item-title -->
                                        <div class="view fa fa-eye">{{ $custom->views }}</div>
                                    </div><!-- /.filtr-item -->
                                </div><!-- /.col -->
                            @endforeach


                        </div><!-- /.row -->
                    </div><!-- /.filtr-container-->
                </div><!-- /.gallery-content -->
            </div>
    </section>
    @include("content.subscribe")

@endsection
