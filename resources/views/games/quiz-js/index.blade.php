<!DOCTYPE html>
<html>

<head>
    <title>
        Giải đố
    </title>

    <!-- (A) LOAD QUIZ CSS + JS -->
    <link href="/games/assets/quiz.css" rel="stylesheet">
    <script src="/games/assets/quiz.js"></script>
</head>

<body>

    <button class="myButton" role="button" style="text-align: text-left; position: fixed"><a
            href="{{ route('games') }}">Quay lại</a></button>
    <!-- (B) QUIZ CONTAINER -->
    <div id="quizWrap"></div>
    <audio id="audioplayer" autoplay>
        <source src="/games/assets/bgm.mp3" type="audio/mpeg">
    </audio>
</body>

</html>
