@extends('mainLayout')
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="main-form">
                    <h3>Thêm phong tục tập quán</h3>
                    <br> <br>
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                        <table class="table table-striped">
                            <tbody id="add_more_images">
                                <tr class="row">
                                    <th class="col-md">Tiêu đề</th>
                                    <td class="col-md"><input name="title" type="text" class="form-control" /></td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Tóm tắt</th>
                                    <td class="col-md"><textarea name="short_description" type="text"
                                            class="form-control"> </textarea></td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Nội dung</th>
                                    <td class="col-md" style="height: 200px"><textarea name="content" type="text"
                                            class="form-control" style="height: 200px"></textarea>
                                    </td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Ảnh hiển thị</th>
                                    <td class="col-md">
                                        <input type="file" name="image" class="form-control">
                                    </td>
                                </tr>
                                <hr>
                                <tr class="row">
                                    <th class="col-md">Ảnh nội dung</th>
                                    <td class="col-md">
                                        <input type="file" name="images[]" class="form-control">
                                    </td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Caption</th>
                                    <td class="col-md">
                                        <input type="text" name="titles[]" class="form-control">
                                    </td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Content</th>
                                    <td class="col-md">
                                        <textarea type="text" name="contents[]" class="form-control"> </textarea>
                                    </td>
                                </tr>
                                <tr class="row">
                                    <th class="col-md">Gửi mail tới người đăng ký</th>
                                    <td class="form-check col-md">
                                        <input class="form-check-input" type="radio" name="yes"
                                            id="flexRadioDefault1">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Có
                                        </label>
                                        <p></p>
                                        <input class="form-check-input" type="radio" name="no"
                                            id="flexRadioDefault1" checked>
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Không
                                        </label>
                                    </td>
                                </tr>
                            </tbody>

                            <tr class="row">
                                <th class="col-md">
                                    <button class="btn btn-warning" type="button" id="addImage">Thêm ảnh</button>
                                </th>
                                <td class="col-md">

                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-primary" name="addOne" value="one">Xác nhận</button>
                        <button type="submit" class="btn btn-success" name="addMany" value="many">Xác nhận và thêm
                            tiếp</button>
                        <div class="btn btn-info">
                            <a href="{{ route('manageCustoms') }}" style="color: white">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#addImage").click(function() {
                let image = " <tr class='row'> " +
                    "<th class='col-md'>Ảnh nội dung</th> " +
                    "<td class='col-md'>" +
                    "<input type='file' name='images[]' class='form-control'>" +
                    " </td></tr>" +
                    " <tr class='row'> " +
                    "<th class='col-md'>Caption</th> " +
                    "<td class='col-md'>" +
                    "<input type='text' name='titles[]' class='form-control'>" +
                    " </td></tr>" +
                    " <tr class='row'> " +
                    "<th class='col-md'>Content</th> " +
                    "<td class='col-md'>" +
                    "<input type='text' name='contents[]' class='form-control'>" +
                    " </td></tr>";
                $("#add_more_images").append(image);
            })
        })
    </script>
@endsection
