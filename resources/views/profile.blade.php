@extends('mainLayout')
@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
            'style' =>"background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1640088096/Hnet.com-image_4_qowei4.jpg')",
            'banner_name' => 'Cài đặt tài khoản'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->


    <section  id="gallery" class="packages">
        <div class="container">
            <div class="gallary-header text-center">
                <h3>Quản lý tài khoản</h3>
                <br> <br>
                <div class="main">
                    <form action="{{route('changePassword')}}" method="post">
                        @csrf
                        <table class="table table-striped">
                            <tr class="row">
                                <th class="col-md-4">Username</th>
                                <td class="col-md-8"><input type="text" name="username" class="form-control" value="{{ $user->username }}" disabled /></td>
                                <input type="hidden" name="username" class="form-control" value="{{ $user->username }}"  />
                            </tr>
                            <tr class="row">
                                <th class="col-md-4">Mật khẩu cũ</th>
                                <td class="col-md-8"><input type="password" name="old_password" class="form-control" placeholder="******"/></td>
                            </tr>
                            <tr class="row">
                                <th class="col-md-4">Mật khẩu mới</th>
                                <td class="col-md-8"><input type="password" name="new_password" class="form-control" placeholder=""/></td>
                            </tr>
                        </table>
                        <div class="text-left">
                            <button class="btn btn-primary" type="submit">Xác nhận</button>
                            <button class="btn btn-danger" type="button"><a href="{{ route("home") }}" style="color: white">Quay về</a></button>
                        </div>
                    </form>
                </div>

            </div>

            <!--/.gallery-header-->
            <div class="packages-content">
                <div class="row">
                </div>
            </div>
        </div>

    </section>

    @include("content.subscribe")
@endsection
