@extends('mainLayout')
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="main-form">
                    <h3>Thêm bài viết mới</h3>
                    <br> <br>
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                        <table class="table table-striped">
                            <tbody id="add_more_images">
                                <tr class="row">
                                    <th class="col-md">Nội dung</th>
                                    <td class="col-md" style="height: 200px"><textarea name="content" type="text"
                                            class="form-control" style="height: 200px"></textarea>
                                    </td>
                                </tr>
                                <hr>
                                <tr class="row">
                                    <th class="col-md">Ảnh đính kèm</th>
                                    <td class="col-md">
                                        <input type="file" name="images[]" class="form-control">
                                    </td>
                                </tr>
                            </tbody>

                            <tr class="row">
                                <th class="col-md">
                                    <button class="btn btn-warning" type="button" id="addImage">Thêm ảnh</button>
                                </th>
                                <td class="col-md">
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-primary" name="addOne" value="one">Xác nhận</button>
                        <div class="btn btn-info">
                            <a href="{{ route('manageCustoms') }}" style="color: white">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#addImage").click(function() {
                let image = " <tr class='row'> " +
                    "<th class='col-md'>Ảnh đính kèm</th> " +
                    "<td class='col-md'>" +
                    "<input type='file' name='images[]' class='form-control'>" +
                    " </td></tr>";
                $("#add_more_images").append(image);
            })
        })
    </script>
@endsection
