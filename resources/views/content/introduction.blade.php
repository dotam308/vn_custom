<section id="gallery" class="gallery">
    <div class="container">
        <div class="gallery-details">
            <div class="gallary-header text-center">
                <h2  style="color: rgb(99,87,128)">
                    Tổng quan về dân tộc Thái
                </h2>
                <p>
                    Là một trong 54 dân tộc đang cộng cư, làm việc, và sinh sống tại mảnh đất Việt Nam yêu thương
                </p>
            </div>
            <br><br>
            <div class="content">
                <p class="timesNew">
                    Người Thái với tên tự gọi là Tãy/Tay/Tày/Thay tùy thuộc vào cách phát âm của từng khu vực. Các nhóm,
                    ngành lớn của người Thái tại Việt Nam bao gồm: Tay Đón (Thái Trắng), Tay Đăm (Thái Đen), Tay Đèng
                    (Thái Đỏ) và Tay Dọ (Thái Yo) cùng một số khác nhỏ hơn. Họ đã có mặt ở miền Tây Bắc Việt Nam trên
                    1200 năm, là hậu duệ những người Thái đã di cư từ vùng đất thuộc tỉnh Vân Nam, Trung Quốc bây giờ.
                    <br>
                    Họ đã tạo ra những nét văn hóa, những phong tục tập quán rất độc đáo, và thú vị. Chúng ra hãy cùng khám phá nhé!
                </p>
            </div>

        <div class="owl-carousel owl-theme" id="testemonial-carousel">

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639553422/trang-phuc-dan-toc-thai_1_otcdjk.jpg" alt="img"/>
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Trang phục truyền thống của dân tộc Thái với chiếc khăn Piêu
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639554753/Thai_Den_227_1_a9mul7.jpg" alt="img"/>
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Nhà sàn truyền thông của dân tộc Thái
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639553505/tungcon2-1_1_oigsjl.jpg" alt="img" />
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Tung Còn, trò chơi truyền thống dân tộc Thái
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639553623/42tra-3-Custom_1_w2f8b7.jpg" alt="img" />
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Cô gái Thái dệt cửi. Bản Lác, Mai Châu, Hòa Bình.
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639553753/Mua_sap_1_tj6wy5.jpg" alt="img" />
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Múa sạp trong lễ hội của người Thái tại Mai Châu, Hòa Bình.
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639555097/images1343214_2017_01_27_nguoi_t_bricx1.jpg" alt="img" />
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Phụ nữ người dân tộc Thái quây quần làm bánh Tết.
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639555449/0_xwkc_1_oqbrgr.jpg" alt="img" />
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Lễ gội đầu là lễ quan trọng mở đầu của các lễ hội trong năm.
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item" style="min-height: 608px">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639554300/kham-pha-tuc-cuoi-xin-truyen-tho_jcrifv.jpg" alt="img"/>
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Các cô gái Thái đen trước khi về nhà chồng phải biết thêu thùa
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

            <div class="home1-testm item">
                <div class="home1-testm-single text-center">
                    <div class="home1-testm-img">
                        <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639554340/kham-pha-tuc-cuoi-xin-truyen-tho_1_oniriq.jpg" alt="img" />
                    </div>
                    <!--/.home1-testm-img-->
                    <div class="home1-testm-txt">
                        <span class="icon section-icon">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </span>
                        <p>
                            Búi tóc đỉnh đầu là biểu tượng của người phụ nữ có chồng
                        </p>
                    </div>
                    <!--/.home1-testm-txt-->
                </div>
                <!--/.home1-testm-single-->

            </div>
            <!--/.item-->

        </div>
        </div>
    </div>
</section>
