<section id="gallery" class="gallery">
    <div class="container">
        <div class="gallery-details">
            <div class="gallary-header text-center">
                <h2>
                    Phong tục tập quán nổi bật
                </h2>
                <p>
                    Cười là loại mĩ phẩm rẻ nhất, vận động là loại y dược rẻ nhất, chào hỏi là loại chi phí giao tiếp rẻ
                    nhất
                </p>
            </div>

            <div class="btn">
                <a class="btn about-view packages-btn" href="{{ route('allCustoms') }}" style="color: white">
                    Xem tất cả
                </a>
            </div>
            <!--/.gallery-header-->
            <div class="gallery-box">
                <div class="gallery-content">
                    <div class="filtr-container">
                        <div class="row">
                            <?php
                            $colMdOrder = [6, 6, 4, 4, 4, 8, 4];
                            $index = 0;
                            ?>
                            @foreach ($customs as $custom)

                                <div class="col-md-{{ $colMdOrder[$index] }}">
                                    @if (session('role') && session('role') == 'admin')
                                        <a class="fa fa-edit"
                                            href="{{ route('editCustom', ['id' => $custom->id]) }}"></a>
                                        <a class="fa fa-trash" style="color: red"
                                            href="{{ route('deleteCustom', ['id' => $custom->id]) }}"></a>
                                    @endif
                                    <div class="filtr-item">
                                        <img src="{{ $custom->displayedImage ? $custom->displayedImage->directory : '' }}"
                                            alt="portfolio image" />
                                        <div class="item-title">
                                            <div>
                                                <a href="{{ route('detailCustom', $custom->id) }}">
                                                    {{ $custom->title }}
                                                </a>
                                            </div>
                                        </div><!-- /.item-title -->
                                        <div class="view fa fa-eye">{{ $custom->views }}</div>
                                    </div><!-- /.filtr-item -->
                                </div><!-- /.col -->
                                <?php
                                $index++;
                                if ($index == 7) {
                                    break;
                                }
                                ?>
                            @endforeach
                        </div><!-- /.row -->

                    </div><!-- /.filtr-container-->
                </div><!-- /.gallery-content -->
            </div>
            <!--/.galley-box-->
        </div>
        <!--/.gallery-details-->
    </div>
    <!--/.container-->

</section>
<!--/.gallery-->
