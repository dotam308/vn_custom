<!--subscribe start-->
<section id="subs" class="subscribe">
    <div class="container">
        <div class="subscribe-title text-center">
            <h2>
                Đăng ký nhận thông báo để nhận được thông tin mới nhất
            </h2>
            <p>
                Đăng ký thôi. Chúng ta sẽ có những trải nghiệm tốt nhất
            </p>
        </div>
        <form method="post" action="{{ route("subscribe") }}">
            @csrf
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="custom-input-group">
                        <input type="email" class="form-control" placeholder="Nhập email ở đây" name="email">
                        <button class="appsLand-btn subscribe-btn">Đăng ký</button>
                        <div class="clearfix"></div>
                        <i class="fa fa-envelope"></i>
                    </div>

                </div>
            </div>
        </form>
    </div>

</section>
<!--subscribe end-->
