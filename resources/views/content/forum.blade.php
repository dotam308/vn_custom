<section id="gallery" class="gallery">
    <div class="container">
        <div class="gallery-details">
            <div class="gallary-header text-center">
                <h2>
                    Diễn đàn trao đổi
                </h2>
                <p>
                    Nơi chia sẻ những trải nghiệm, thông tin về dân tộc Thái
                </p>
            </div>

            <div class="btn">
                <a class="btn about-view packages-btn" href="{{ route('forum') }}" style="color: white">
                    Xem thêm
                </a>
            </div>
            <!--/.gallery-header-->
            <div class="gallery-box">
                <div class="gallery-content">
                    <div class="filtr-container">
                        <div class="row">

                            <td class="col-md" style=>
                                <a href="{{ route('startGame', ['id' => 1]) }}"><img width="560" height="315"
                                        src="https://res.cloudinary.com/dbiexlh94/image/upload/v1640080932/hinh1_y8imaz.png" /></a>
                            </td>
                            <td class="col-md">
                                <a href="{{ route('startGame', ['id' => 2]) }}"><img width="560" height="315"
                                        src="https://res.cloudinary.com/dbiexlh94/image/upload/v1640080932/hinh2_w4thxm.png" /></a>
                            </td>
                        </div><!-- /.row -->

                    </div><!-- /.filtr-container-->
                </div><!-- /.gallery-content -->
            </div>
        </div>
        <!--/.gallery-details-->
    </div>
    <!--/.container-->

</section>
<!--/.gallery-->
