<section id="gallery" class="gallery">
    <div class="container">
        <div class="gallery-details">
            <div class="gallary-header text-center">
                <h2>
                    Một số thước phim sưu tập liên quan
                </h2>
                <p>
                    Cười là loại mĩ phẩm rẻ nhất, vận động là loại y dược rẻ nhất, chào hỏi là loại chi phí giao tiếp rẻ
                    nhất
                </p>
            </div>

            <div class="btn">
                <a class="btn about-view packages-btn" href="{{ route('allVideos') }}" style="color: white">
                    Xem thêm
                </a>
            </div>
            <div>

                <iframe width="560" height="315" src="https://www.youtube.com/embed/p1UvGFvGpts"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/-1XRms-iPbY"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            </div>
        </div>
        <!--/.gallery-details-->
    </div>
    <!--/.container-->

</section>
<!--/.gallery-->
