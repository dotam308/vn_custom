<section id="home" class="about-us" style="{{ isset($customBanner['style']) ? $customBanner['style'] : ""}}">
    <div class="container">
        <div class="about-us-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="single-about-us">
                        <div class="about-us-txt">
                            <h2 style="{{ isset($customBanner['position']) ? $customBanner['position'] : ""}}">

                                {{ isset($customBanner['banner_name']) ? $customBanner['banner_name'] : "Khám phá vẻ đẹp của dân tộc Thái" }}
                            </h2>
                            <div class="about-btn smooth-menu">
                                <a class="about-view btn smooth-menu" type="button" href="#gallery">
                                    {{ isset($customBanner['slogan']) ? $customBanner['slogan'] : "Bắt đầu nào" }}

                                </a>
                            </div>
                            <!--/.about-btn-->
                        </div>
                        <!--/.about-us-txt-->
                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
                <div class="col-sm-0">
                    <div class="single-about-us">

                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
