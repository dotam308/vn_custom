<section id="pack" class="packages">
    <div class="container">
        <div class="gallary-header text-center">
            <h2>
                Ẩm thực độc đáo
            </h2>
            <p>
                Khám phá ẩm thực người Thái
            </p>
        </div>

        <div class="btn">
            <a class="btn about-view packages-btn" href="{{ route('allCuisines') }}" style="color: white">Xem tất
                cả</a>
        </div>
        <div class="packages-content">
            <div class="row">
                @foreach ($cuisines as $cuisine)

                    <div class="col-md-4 col-sm-6">
                        @if (session('role') && session('role') == 'admin')
                            <a class="fa fa-edit" href="{{ route('editCuisine', ['id' => $cuisine->id]) }}"></a>
                            <a class="fa fa-trash" style="color: red"
                                href="{{ route('deleteCuisine', ['id' => $cuisine->id]) }}"></a>
                        @endif
                        <div class="single-package-item">
                            <img src="{{ $cuisine->displayedImage ? $cuisine->displayedImage->directory : "" }}" alt="package-place">
                            <div class="single-package-item-txt">
                                <h3>{{ $cuisine->title }} <span class="pull-right"></span></h3>
                                <div class="packages-para">

                                    <p>
                                        {{ $cuisine->short_description }}
                                    </p>
                                </div>
                                <div class="view fa fa-eye">{{ $cuisine->views }}</div>
                                <!--/.packages-review-->
                                <div class="about-btn">
                                    <a class="btn about-view packages-btn"
                                        href="{{ route('detailCuisine', ['id' => $cuisine->id]) }}"
                                        style="color: white">Chi tiết</a>
                                </div>
                                <!--/.about-btn-->
                            </div>
                            <!--/.single-package-item-txt-->
                        </div>
                        <!--/.single-package-item-->

                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!--/.container-->

</section>
<!--/.packages-->
