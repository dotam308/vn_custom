<section id="gallery" class="gallery">
    <div class="container">
        <div class="gallery-details">
            <div class="gallary-header text-center">
                <h2>
                    Trò chơi của ThaiTour
                </h2>
                <p>
                    Một cách tiếp cận khác với những thông tin bổ ích
                </p>
            </div>

            <div class="btn">
                <a class="btn about-view packages-btn" href="{{ route('games') }}" style="color: white">
                    Xem thêm
                </a>
            </div>
            <!--/.gallery-header-->
            <div class="gallery-box">
                <div class="gallery-content">
                    <div class="filtr-container">
                        <div class="row">

                            <td class="col-md">

                                <a href="{{ route('startGame', ['id' => 1]) }}"><img width="560" height="315"
                                        src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639975444/quiz_dkjud6.png" /></a>
                            </td>
                            <td class="col-md">
                                <a href="{{ route('startGame', ['id' => 2]) }}"><img width="560" height="315"
                                        src="https://res.cloudinary.com/dbiexlh94/image/upload/v1640079860/dfn_kvq9xt.png" /></a>
                            </td>
                        </div><!-- /.row -->

                    </div><!-- /.filtr-container-->
                </div><!-- /.gallery-content -->
            </div>
        </div>
        <!--/.gallery-details-->
    </div>
    <!--/.container-->

</section>
<!--/.gallery-->
