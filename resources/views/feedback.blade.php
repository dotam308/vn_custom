@extends('mainLayout')
@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
        'style' => "background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1640103129/Hnet.com-image_5_mhk18l.jpg')",
        'banner_name' => 'Hãy cho chúng tôi biết suy nghĩ của bạn',
        'position' => 'text-right'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="main-form">
                    <h3>Gửi phản hồi</h3>
                    <br> <br>
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                        <table class="table table-striped">
                            <tr class="row">
                                <th class="col-md">Email của bạn</th>
                                <td class="col-md"><input name="email" type="text" class="form-control" /></td>
                            </tr>
                            <tr class="row">
                                <th class="col-md">Tiêu đề</th>
                                <td class="col-md"><textarea name="title" type="text"
                                        class="form-control"> </textarea></td>
                            </tr>
                            <tr class="row">
                                <th class="col-md">Nội dung</th>
                                <td class="col-md" style="height: 200px"><textarea name="content" type="text"
                                        class="form-control" style="height: 200px"></textarea>
                                </td>
                            </tr>
                        </table>

                        <button type="submit" class="btn btn-primary" name="addOne" value="one">Xác nhận</button>
                        <button class="btn btn-info"><a href="{{ route('home') }}" style="color: white">Quay lại</a></button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#addImage").click(function() {
                let image = " <tr class='row'> " +
                    "<th class='col-md'>Ảnh nội dung</th> " +
                    "<td class='col-md'>" +
                    "<input type='file' name='images[]' class='form-control'>" +
                    " </td></tr>" +
                    " <tr class='row'> " +
                    "<th class='col-md'>Caption</th> " +
                    "<td class='col-md'>" +
                    "<input type='text' name='titles[]' class='form-control'>" +
                    " </td></tr>" +
                    " <tr class='row'> " +
                    "<th class='col-md'>Content</th> " +
                    "<td class='col-md'>" +
                    "<input type='text' name='contents[]' class='form-control'>" +
                    " </td></tr>";
                $("#add_more_images").append(image);
            })
        })
    </script>
@endsection
