@extends('mainLayout')
@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
            'style' =>"background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639566742/Hnet.com-image_1_agb3lp.jpg')",
            'banner_name' => 'Hãy cùng thưởng thức những đặc sản của dân tộc Thái'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->


    <section  id="gallery" class="packages">
        <div class="container">
            <div class="gallary-header text-center">
                <div class="search">
                    <br><br><br><br>
                    <form action="{{route('cuisineSearch')}}" method="get">
                        <div class="col-md-4 col-sm-6" style="width:615px">
                            <div class="form-group">
                                <input class="form-control" placeholder="Nhập từ khoá" id="keywords"
                                    name="keywords" type="text">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="submit" value="Tìm kiếm" style="background-color: rgb(255, 94, 0); color: #fff;">
                            </div>
                        </div>
                    </form>
                </div>
                <br><br><br>
                <h2>
                    Ẩm thực độc đáo
                </h2>
                <p>
                    Khám phá ẩm thực người Thái
                </p>
            </div>

            <!--/.gallery-header-->
            <div class="packages-content">
                <div class="row">
                    @foreach ($cuisines as $cuisine)

                        <div class="col-md-4 col-sm-6">
                            @if (session('role') && session('role') == 'admin')
                                <a class="fa fa-edit" href="{{ route('editCuisine', ['id' => $cuisine->id]) }}"></a>
                                <a class="fa fa-trash" style="color: red"
                                    href="{{ route('deleteCuisine', ['id' => $cuisine->id]) }}"></a>
                            @endif
                            <div class="single-package-item">
                                <img src="{{ $cuisine->displayedImage ? $cuisine->displayedImage->directory : "\images\xoi_ngu_sac.jpg" }}" alt="package-place">
                                <div class="single-package-item-txt">
                                    <h3>{{ $cuisine->title }} <span class="pull-right"></span></h3>
                                    <div class="packages-para">

                                        <p>
                                            {{ $cuisine->short_description }}
                                        </p>
                                    </div>
                                    <div class="view fa fa-eye">{{ $cuisine->views }}</div>
                                    <!--/.packages-review-->
                                    <div class="about-btn">
                                        <div class="about-btn">
                                            <a class="btn about-view packages-btn"
                                                href="{{ route('detailCuisine', ['id' => $cuisine->id]) }}"
                                                style="color: white">Chi tiết</a>
                                        </div>
                                    </div>
                                    <!--/.about-btn-->
                                </div>
                                <!--/.single-package-item-txt-->
                            </div>
                            <!--/.single-package-item-->

                        </div>
                    @endforeach
                </div>
            </div>
            @include('includes.navigation', ['data'=>$cuisines])
        </div>

    </section>

    @include("content.subscribe")
@endsection
