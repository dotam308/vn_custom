@extends('mainLayout')

@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
            'style' =>"background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639571882/vid2_e1qjit.png')",
            'banner_name' => 'Hiểu hơn về dân tộc Thái qua các videos'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <br><br><br><br>
            <form action="{{route('customSearch')}}" method="get">
                <div class="col-md-4 col-sm-6" style="width:615px">
                    <div class="form-group">
                        <input class="form-control" placeholder="Nhập từ khoá" id="keywords"
                            name="keywords" type="text">
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="form-group">
                        <input class="form-control" type="submit" value="Tìm kiếm" style="background-color: rgb(255, 94, 0); color: #fff;">
                    </div>
                </div>
            </form>
            <br>
            <div class="gallery-details">
                <div class="gallary-header text-center">
                    <h2 class="timesNew">
                        {{ $video->short_description }}
                    </h2>
                </div>
                <div class="text-center">

                    <iframe width="560" height="315" src="{{ $video->src }}"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                    <p class="timesNew">{{ $video->content }}</p>
            </div>
            <!--/.gallery-details-->

            <section id="comment"></section>
            <div class="fb-comments" data-href="{{ env('APP_URL') }}detail-video/{{ $video->id }}#comment"
            data-width="1000" data-numposts="5"></div>
            <br><br><br>
            <h2>Có thể bạn quan tâm</h2>
            <div class="gallery-box">
                <div class="gallery-content">
                    <div class="filtr-container">
                        <div class="row">
                            @foreach ($relatedVideos as $video)
                                <div class="col-md-6">
                                    <div class="filtr-item">

                                        <img src="{{ $video->image_url ? $video->image_url : '\images\xoi_ngu_sac.jpg' }}"
                                            alt="portfolio image" />
                                        <div class="item-title">
                                            <a href="{{ route('detailVideo', $video->id) }}">
                                                {{ $video->short_description }}
                                            </a>
                                        </div><!-- /.item-title -->
                                    </div><!-- /.filtr-item -->
                                </div><!-- /.col -->
                            @endforeach


                        </div><!-- /.row -->
                    </div><!-- /.filtr-container-->
                </div><!-- /.gallery-content -->
            </div>
        </div>
        <!--/.container-->

    </section>
    <!--/.gallery-->

    @include("content.subscribe")
@endsection
