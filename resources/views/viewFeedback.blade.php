@extends('mainLayout')
@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
            // 'style' =>"background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639566742/Hnet.com-image_1_agb3lp.jpg')",
            'banner_name' => 'Quản lý các lượt phản hồi'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="main-form">
                    <h3>Danh sách phản hồi</h3>
                    <br> <br>

                    <table class="table table-striped">
                        <tr class="row timesNew px18">
                            <th class="col-md">Code</th>
                            <th class="col-md">Email</th>
                            <th class="col-md">Tiêu đề</th>
                            <th class="col-md">Nội dung</th>
                            <th class="col-md">Action</th>
                        </tr>
                        @foreach ($feedbacks as $feedback)
                            <tr class="row timesNew px18">
                                <td class="col-md">{{ $feedback->id }}</td>
                                <td class="col-md">{{ $feedback->email }}</td>
                                <td class="col-md">{{ $feedback->title }}</td>
                                <td class="col-md">{{ $feedback->content }}</td>
                                <td class="col-md">
                                    <button class="btn btn-info"><a href="{{ route('handleFeedback', $feedback->id) }}" style="color: white">Xử lý</a></button>
                                </td>
                            </tr>

                        @endforeach

                    </table>
                @include('includes.navigation', ['data'=>$feedbacks])
                </div>
            </div>
        </div>
    </section>
@endsection
