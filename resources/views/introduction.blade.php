@extends('mainLayout')
@section('style')
    <style type="text/css">
        .black {
            color: black !important;
            font-size: : 20px !important;
        }


        h4,
        h3 {
            font-weight: 1000 !important;
        }

        p.menu {
            font-size: 12px !important;
            font-weight: 200px !important;
        }

        h4 {
            color: red;
        }

        .menu {
            background-color: #FFFFCC !important;
        }

        .link-menu {
            font-size: 20px !important;
        }

    </style>
@endsection
@section('content')

    <!--about-us start -->

    <?php
    $customBanner = [
            'style' =>"background: url('/assets/images/home/banner_gioithieu.png')",
            'banner_name' => 'Vài nét về dân tộc Thái'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--/.about-us-->

    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="gallary-header">
                    <h2 class="text-center">Người Thái(Việt Nam)</h2>
                </div>
                <div class="gallary-header">
                    <div class="text-center">
                        <img style="margin: 5px"
                            src="http://lichsuvanhoathai.com/wp-content/uploads/2020/02/avt-dac-trung-nguoi-thai.png"
                            alt="Người dân tộc Thái" />
                        <figcaption>Src:
                            http://lichsuvanhoathai.com/tai-lieu/nhung-dac-trung-co-ban-cua-nguoi-thai-viet-nam/
                        </figcaption>
                        <br>
                    </div>
                    <p class="text-left" style="color: black">Người Thái với tên tự gọi là Tãy/Tay/Tày/Thay tùy thuộc
                        vào cách phát âm của
                        từng khu vực. Các nhóm,
                        ngành lớn của người Thái tại Việt Nam bao gồm: Tay Đón (Thái Trắng), Tay Đăm (Thái Đen), Tay Đèng
                        (Thái Đỏ) và Tay Dọ (Thái Yo) cùng một số khác nhỏ hơn. Họ đã có mặt ở miền Tây Bắc Việt Nam trên
                        1200 năm, là hậu duệ những người Thái đã di cư từ vùng đất thuộc tỉnh Vân Nam, Trung Quốc bây giờ
                    </p>
                    <br>
                    <div class="menu" style="border: 1px solid black">
                        <p class="text-center" style="color: black"><b>Mục lục</b></p>
                        <p><a href="#history" class="link-menu  smooth-menu">1. Lịch sử</a></p>
                        <p><a href="#population" class="link-menu smooth-menu">2. Dân số</a></p>
                        <p><a href="#economy" class="link-menu smooth-menu">3. Đặc điểm kinh tế</a></p>
                    </div>
                    <div class="content">
                        <div class="title">
                            <br><br>
                            <h4 class="text-left" id="history">1. Lịch sử</h4>
                            <div class="text-center">
                                <img src="https://4.bp.blogspot.com/-LVOwJ1PvVmc/VxFLIRIfxoI/AAAAAAAACaE/fIUlPVDXSrkZYsVsM8i7z_JRz4wCu0VNwCLcB/s1600/baotrovanhoaVN%2B005-162.jpg"
                                    alt="img" />
                                <figcaption>Src:
                                    https://vanhoanvietnam.blogspot.com/2016/04/dan-ca-thai-cua-dan-toc-thai-van-hoa.html
                                </figcaption>
                            </div>
                            <p class="text-left black">
                                Dựa trên các chứng cứ về di truyền, Jerold A. Edmondson cho rằng tổ tiên
                                của các cư dân nói ngôn ngữ Tai-Kadai di cư từ Ấn Độ tới Myanmar, rồi sau đó tới Vân Nam
                                (Trung
                                Quốc) khoảng 20.000 - 30.000 năm trước.Từ đó họ đến đông bắc Thái Lan và sau đó di cư dọc
                                vùng duyên hải Hoa Nam lên phía bắc đến cửa sông Trường Giang, gần Thượng Hải khoảng
                                8-10.000
                                năm trước. Vào thời kỳ vương quốc Nam Chiếu và Đại Lý tồn tại từ TK 8 đến TK 13, cũng như
                                sau đó, họ từ đó chiếm lĩnh Thái Lan và Lào. Tuy nhiên, những chứng cứ trên của ông bị bác
                                bỏ vì
                                nó chưa đúng do không có người Thái di cư ngược sang sông Trường Giang.
                                <br>
                            </p>

                            <div class="text-center">
                                <img src="https://i.ebayimg.com/images/g/QK4AAOSw8SFf~~si/s-l500.jpg" alt="img" />
                                <figcaption>Src: https://www.ebay.com/p/114716</figcaption>
                            </div>
                            <p class="text-left black">
                                Theo David Wyatt, trong cuốn "Thailand: A short history (Thái Lan: Lịch sử Tóm lược)", người
                                Thái xuất xứ từ phía nam Trung Quốc, có cùng nguồn gốc với các nhóm dân ít người bây giờ như
                                Choang, Tày, Nùng. Dưới sức ép của người Hán và người Việt ở phía bắc và phía đông, người
                                Thái
                                dần di cư về phía nam và tây nam. Người Thái di cư đến Việt Nam trong thời gian từ thế kỷ
                                VII
                                đến thế kỷ XIII. Trung tâm của họ khi đó là Điện Biên Phủ (Mường Thanh). Từ đây, họ tỏa đi
                                khắp nơi ở Đông Nam Á cùng 1 lúc bây giờ như Lào, Thái Lan, bang Shan ở Miến Điện và một số
                                vùng
                                ở đông bắc Ấn Độ cũng như nam Vân Nam. Vậy nên tổ tiến của những Thái đều ở Vân Nam hết rồi
                                di cư xuống Đông Nam Á và Ấn Độ.
                                <br>
                                Theo sách sử Việt Nam, vào thời Nhà Lý, man Ngưu Hống (được cho là một cộng đồng người Thái
                                Đen,
                                đây là âm Hán Việt phiên từ tiếng Thái: ngũ háu - tức là "rắn Hổ mang") ở đạo Đà Giang,
                                đã triều cống lần đầu tiên vào năm 1067. Trong thế kỷ XIII, người Ngưu Hống kết hợp với
                                người Ai
                                Lao chống lại Nhà Trần và bị đánh bại. Năm 1280, một thủ lĩnh tên Trịnh Giác Mật, được gọi
                                là
                                "chúa đạo Đà Giang" đầu hàng Chiêu Văn vương Trần Nhật Duật nhà Trần, nhưng chưa rõ thành
                                phần
                                dân tộc của nhân vật này. Năm 1337 lãnh tụ Xa Phần bị giết chết sau một cuộc xung đột, xứ
                                Ngưu
                                Hống bị sáp nhập vào lãnh thổ Đại Việt và đổi tên thành Mường Lễ, hay Ninh Viễn (Lai Châu
                                ngày
                                nay) và giao cho họ Đèo cai quản. Năm 1431 lãnh tụ Đèo Cát Hãn, người Thái Trắng tại Mường
                                Lễ
                                (tức Mường Lay, nổi lên chống triều đình, chiếm hai lộ Qui Hóa (Lào Cai) và Gia Hưng (giữa
                                sông
                                Mã và sông Đà), tấn công Mường Mỗi (tức Mường Muổi, Sơn La), Đèo Mạnh Vương (con của Đèo Cát
                                Hãn) làm tri châu. Năm 1466, lãnh thổ vùng tây bắc Đại Việt, gồm những vùng đất của người
                                Thái
                                được tổ chức lại thành thừa tuyên Hưng Hóa, gồm 3 phủ: An Tây (tức Phục Lễ), Gia Hưng và Qui
                                Hóa, 4 huyện và 17 châu.
                            </p>
                            <br>

                            <div class="text-center">
                                <img src="https://media.ex-cdn.com/EXP/media.nongnghiep.vn/files/f1/2018/7/3/19-56-22_t1.jpg"
                                    alt="img" />
                                <figcaption>Chân dung Đèo Văn Long - "ông vua" phương Bắc</figcaption>
                                <figcaption>Src:
                                    https://nongnghiep.vn/chuyen-ve-nhung-ong-vua-mien-nui-phia-bac-deo-van-long-o-xu-thai-d221855.html
                                </figcaption>
                            </div>
                            <p class="text-left black">
                                Những lãnh tụ Thái được gọi là phụ tạo, được phép cai quản một số lãnh địa và trở thành giai
                                cấp
                                quý tộc của vùng đó, như dòng họ Đèo cai quản các châu Lai, Chiêu Tấn, Tuy Phụ, Hoàng Nham;
                                dòng
                                họ Cầm các châu Phù Hoa, Mai Sơn, Sơn La, Tuần Giáo, Luân, Ninh Biên; dòng họ Xa cai quản
                                châu
                                Mộc; dòng họ Hà cai quản châu Mai, dòng họ Bạc ở châu Thuận; họ Hoàng ở châu Việt...
                                <br>
                                Năm 1841, trước sự đe dọa của người Xiêm La, triều đình Nhà Nguyễn kết hợp ba châu Ninh
                                Biên,
                                Tuần Giáo và Lai Châu ở tả ngạn sông Mekong thành phủ Điện Biên. Năm 1880, phó lãnh sự Pháp
                                là
                                Auguste Pavie nhân danh triều đình Việt Nam phong cho Đèo Văn Trị chức tri phủ cha truyền
                                con
                                nối tại Điện Biên; sau khi giúp người Pháp xác định khu vực biên giới giữa Việt Nam với
                                Trung
                                Quốc và Lào, Đèo Văn Trị được cử làm quan của đạo Lai Châu, cai quản một lãnh thổ rộng lớn
                                từ
                                Điện Biên Phủ đến Phong Thổ, còn gọi là xứ Thái. Tháng 3, 1948 lãnh thổ này được Pháp tổ
                                chức
                                lại thành Liên bang Thái tự trị, qui tụ tất cả các sắc tộc nói tiếng Thái chống lại Việt
                                Minh.
                                <br>
                                Sau chiến thắng Điện Biên Phủ, để lấy lòng các sắc tộc thiểu số miền Bắc, Chính phủ Việt Nam
                                Dân
                                chủ Cộng hòa thành lập Khu tự trị Thái Mèo ngày 29 tháng 4 năm 1955, Khu tự trị Tày Nùng và
                                vùng
                                tự trị Lào Hạ Yên, nhưng tất cả các khu này đều bị giải tán năm 1975.
                            </p>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/0O9tO7FRqLY"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                        <br><br><br>
                        <div class="title">
                            <h4 class="text-left" id="population">2. Dân số</h4>
                            <table>
                                <tr class="row">
                                    <td class="col-3">
                                        <p class="text-left" style="color: black">
                                            Tại Việt Nam, theo Tổng điều tra dân số năm 1999, người Thái có số dân là
                                            1.328.725
                                            người, chiếm 1,74% dân số cả nước, cư trú tập trung tại các tỉnh Lai Châu, Điện
                                            Biên,
                                            Lào Cai, Yên Bái, Sơn La, Hòa Bình, Thanh Hóa, Nghệ An (số lượng người Thái tại
                                            8 tỉnh này
                                            chiếm 97,6% tổng số người Thái ở Việt Nam)và một số ở Tỉnh Lâm Đồng và Đắk Lắk.
                                            Trong đó tại
                                            Sơn La có 482.485 người (54,8 % dân số), Nghệ An có 269.491 người (9,4 % dân
                                            số), Thanh Hóa
                                            có 210.908 người (6,1 % dân số), Lai Châu cũ (nay là Lai Châu và Điện Biên) có
                                            206.001 người
                                            (35,1 % dân số).
                                            <br>

                                            Theo Tổng điều tra dân số và nhà ở năm 2009, người Thái ở Việt Nam có dân số
                                            1.550.423
                                            người, là dân tộc có dân số đứng thứ 3 tại Việt Nam, có mặt trên tất cả 63 tỉnh,
                                            thành phố.
                                            Người Thái cư trú tập trung tại các tỉnh:
                                            <br>
                                            -Sơn La [Mường La] (572.441 người, chiếm 53,2% dân số toàn tỉnh và 36,9% tổng số
                                            người
                                            Thái
                                            tại Việt Nam) <br>
                                            -Nghệ An (295.132 người, chiếm 10,1% dân số toàn tỉnh và 19,0% tổng số người
                                            Thái tại
                                            Việt
                                            Nam)<br>
                                            -Thanh Hóa (225.336 người, chiếm 6,6% dân số toàn tỉnh và 14,5% tổng số người
                                            Thái tại
                                            Việt
                                            Nam)<br>
                                            -Điện Biên [Mường Thèng] (186.270 người, chiếm 38,0% dân số toàn tỉnh và 12,0%
                                            tổng số
                                            người
                                            -Thái tại Việt Nam)<br>
                                            -Lai Châu [Mường Lay] (119.805 người, chiếm 32,3% dân số toàn tỉnh và 7,7% tổng
                                            số người
                                            Thái
                                            tại Việt Nam)<br>
                                            -Yên Bái [Mường Lo] (53.104 người)<br>
                                            -Hòa Bình (31.386 người)<br>
                                            -Đắk Lắk (17.135 người)<br>
                                            -Đắk Nông (10.311 người)...<br>
                                        </p>
                                    </td>
                                    <td class="col-9"><iframe
                                            src="https://www.google.com/maps/d/embed?mid=1M4UJrC9S9aqwD8f8nPdcxDWdy7X-gxnP&ehbc=2E312F"
                                            width="640" height="480"></iframe>
                                    </td>
                                </tr>
                            </table>
                            <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639224172/iwrt5kwun9rxe8xgpkeh.png"
                                alt="chart" />
                        </div>
                        <br> <br>
                        <div class="black title">
                            <h4 id="economy">3. Đặc điểm kinh tế</h4> <br>
                            <div class="text-center">
                                <img src="https://namphuongtourist.com/wp-content/uploads/2012/10/toancanhmuongla.jpg"
                                    alt="img" />

                                <img src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639288720/jgmuorpgfvyu4xmn2fep.jpg"
                                    alt="img" />
                                <figcaption>Src: https://namphuongtourist.com</figcaption>
                            </div>
                            <p class="black">Người Thái có nhiều kinh nghiệm, đào mương, dựng đập, bắc máng lấy
                                nước làm ruộng. Lúa nước
                                là nguồn lương thực chính, đặc biệt là lúa nếp. Người Thái cũng làm nương để trồng lúa, hoa
                                màu và nhiều thứ cây khác. Từng gia đình chăn nuôi gia súc, gia cầm, đan lát, dệt vải, một
                                số nơi làm đồ gốm... Sản phẩm nổi tiếng của người Thái là vải thổ cẩm, với những hoa văn độc
                                đáo, màu sắc rực rỡ, bền đẹp</p>
                        </div>
                        <br> <br>
                        <div class="ref">
                            <h2 class="black">Nguồn tham khảo: </h2>
                            <p>
                                <a href="https://vi.wikipedia.org/wiki/Ng%C6%B0%E1%BB%9Di_Th%C3%A1i_(Vi%E1%BB%87t_Nam)">Wikipedia:
                                    Người Thái (Việt Nam)</a>
                            </p>
                            <p>
                                <a href="http://www.cema.gov.vn/gioi-thieu/cong-dong-54-dan-toc/nguoi-thai.htm">cema.gov.vn:
                                    NGƯỜI THÁI</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.gallery-details-->
        </div>
        <!--/.container-->

    </section>
    <!--/.gallery-->

    @include("content.subscribe")
@endsection
