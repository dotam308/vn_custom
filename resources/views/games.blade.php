@extends('mainLayout')

@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
        'style' => "background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1640085100/Hnet.com-image_2_ri84rl.jpg')",
        'banner_name' => 'Vừa giải trí vừa biết thêm về con người nơi đây',
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="gallary-header text-center">
                    <h2>
                        Tổng hợp một số trò chơi
                    </h2>
                    <p>
                        Giải trí một chút với những games của ThaiTour
                    </p>
                    <br><br><br>
                    <table>
                        {{-- @foreach ($videos as $video) --}}
                        <tr class="row">
                            <td class="col-md-6">
                                <a href="{{ route('startGame', ['id' => 1]) }}"><img width="560" height="315"
                                        src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639975444/quiz_dkjud6.png" /></a>
                            </td>
                            <td>
                                <h2 class="timesNew">
                                    <a style="text-decoration: none; font-size: 30px; color: #F05A50" href="{{ route('startGame', ['id' => 1]) }}">Giải câu đố</a></h2>
                                <p class="timesNew black">Người chơi chọn 1 trong những đáp án được cho
                                     <br>
                                     Sau khi chọn, trò chơi sẽ hiển thị đáp án chính xác
                                      <br>
                                      Trò chơi giúp tổng kết các thông tin về dân tộc Thái
                                    </p>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-md-6">
                                <a href="{{ route('startGame', ['id' => 2]) }}"><img width="560" height="315"
                                        src="https://res.cloudinary.com/dbiexlh94/image/upload/v1639975301/tower_suevrg.png" /></a>
                            </td>
                            <td>
                                <h2 class="timesNew">
                                    <a style="text-decoration: none; font-size: 30px; color: #74B6DB" href="{{ route('startGame', ['id' => 1]) }}">Nối hình</a></h2>
                                <p class="timesNew black">

                                    Khi vào trò chơi, người chơi sẽ dùng chuột trái để thả item xuống sao cho khớp với item rơi xuống trước đó
                                    <br>
                                    Item hiển thị ra là 1 trong  các hình ảnh về đồ gốm được làm bởi dân tộc Thái
                                    <br>
                                    Trò chơi giúp người chơi biết về một loại hình kinh tế của dân tộc Thái, đó là đồ gốm.

                                </p>
                            </td>
                        </tr>
                        {{-- @endforeach --}}

                    </table>
                </div>
                {{-- <!--/.gallery-header-->
                <div class="gallery-box">
                    <div class="gallery-content">
                        <div class="filtr-container">
                            <div class="row">


                            </div><!-- /.row -->
                        </div><!-- /.filtr-container-->
                    </div><!-- /.gallery-content -->
                </div>
                <!--/.galley-box--> --}}
            </div>
            <!--/.gallery-details-->
        </div>
        <!--/.container-->

    </section>
    <!--/.gallery-->

    @include("content.subscribe")
@endsection
