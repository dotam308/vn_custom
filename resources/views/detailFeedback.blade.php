@extends('mainLayout')
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->


    <section id="gallery" class="packages">
        <div class="container">
            <form method="post" enctype="multipart/form-data">
                @csrf
                <p class="timesNew"> Phản hồi từ email: {{ $feedback->email}}</p>
                <div class="gallary-header text-center">
                    <h2>
                        {{ $feedback->title }}
                    </h2>
                </div>
                <div>
                    <p class="timesNew">
                        {{ $feedback->content }}
                    </p>
                </div>
                <div>
                    <input type="hidden" name="id" class="form-control" value="{{ $feedback->id }}" >
                    <input type="hidden" name="email" class="form-control" value="{{ $feedback->email }}" >
                </div>
                <br><br><br><br><br>
                <div class="row">
                    <div class="col-md timesNew px18">Câu trả lời của bạn</div>
                    <div class="col-md timesNew" style="height: 200px"><textarea name="content" type="text"
                            class="form-control px18" style="height: 200px"></textarea>
                    </div>
                </div>
                {{-- <div class="row"> --}}
                    <br>
                    <button type="submit" class="btn btn-success" name="sendReply" value="one">Gửi trả lời</button>
                    <button type="submit" class="btn btn-primary" name="markDone" value="many">Đánh dấu hoàn thành</button>
                    <div class="btn btn-info"><a href="{{ route('viewFeedback') }}" style="color: white">Quay lại</a></div>
                {{-- </div> --}}
            </form>
        </div>
    </section>

    @include("content.subscribe")

@endsection
