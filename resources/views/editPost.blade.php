@extends('mainLayout')
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="main-form">
                    <h3>Chỉnh sửa</h3>
                    <br> <br>
                    <form action="{{ route('handleEditPost') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <table class="table table-striped">
                            <tbody id="add_more_images">
                                <input type="hidden" name="id" value="{{ $post->id }}" />
                                <tr class="row">
                                    <th class="col-md">Nội dung:</th>
                                    <td class="col-md" style="height: 200px"><textarea name="content" type="text"
                                            class="form-control" style="height: 200px">{{ $post->content }}</textarea>
                                    </td>
                                </tr>

                                <hr>
                                @forelse ($post->images as  $image)
                                    <tr class="row {{ 'row-' . $image->id }}">
                                        <th class="col-md">Ảnh nội dung
                                            <button class="btn btn-danger" type="button" id="remove-{{ $image->id }}"
                                                data-id="{{ $image->id }}">Xóa
                                            </button>
                                        </th>
                                        <td class="col-md">
                                            <img src="{{ $image->directory }}" />
                                            <input type="file" name="oldImages[]" class="form-control">
                                        </td>
                                    </tr>

                                @empty

                                @endforelse
                            </tbody>

                            <tr class="row">
                                <th class="col-md">
                                    <button class="btn btn-warning" type="button" id="editImage">Thêm</button>
                                </th>
                                <td class="col-md">

                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-primary">Xác nhận</button>
                        <button class="btn btn-info"><a href="{{ route('myPosts') }}">Quay lại</a></button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#editImage").click(function() {
                let image = " <tr class='row'> " +
                    "<th class='col-md'>Ảnh đính kèm</th> " +
                    "<td class='col-md'>" +
                    "<input type='file' name='images[]' class='form-control'>" +
                    " </td></tr>";
                $("#add_more_images").append(image);
            });

            $("button[id^='remove']").click(function() {
                let key = $(this).attr("data-id");
                let row = ".row-" + key;
                let confirmation = confirm("Bạn chắc chắn chứ? Hành động này sẽ không được khôi phục đâu nhé!");
                if (confirmation) {
                    $(row).remove();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "/delete-post-image/" + key,
                        type: "DELETE",
                        contentType: "application/json",
                        success: function() {
                            alert("Hình ảnh đã được xóa thành công.");
                        },
                        error: function(data, textStatus, errorThrown) {
                            console.log(data);

                        },
                    });
                }

            });

        });
    </script>
@endsection
