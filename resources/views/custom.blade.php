@extends('mainLayout')

@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
            'style' =>"background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639566351/Hnet.com-image_otudo7.jpg')",
            'banner_name' => 'Tìm hiểu về những phong tục tập quán của đồng bào dân tộc Thái'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <br><br><br><br>
            <form action="{{ route('customSearch') }}" method="get">
                <div class="col-md-4 col-sm-6" style="width:615px">
                    <div class="form-group">
                        <input class="form-control" placeholder="Nhập từ khoá" id="keywords" name="keywords" type="text">
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="form-group">
                        <input class="form-control" type="submit" value="Tìm kiếm"
                            style="background-color: rgb(255, 94, 0); color: #fff;">
                    </div>
                </div>
            </form>
            <br>
            <div class="gallery-details">
                <div class="gallary-header text-center">
                    <h2>
                        Phong tục tập quán nổi bật
                    </h2>
                    <p>
                        Cười là loại mĩ phẩm rẻ nhất, vận động là loại y dược rẻ nhất, chào hỏi là loại chi phí giao tiếp rẻ
                        nhất
                    </p>
                </div>
                <!--/.gallery-header-->
                <div class="gallery-box">
                    <div class="gallery-content">
                        <div class="filtr-container">
                            <div class="row">
                                @foreach ($customs as $custom)
                                    <div class="col-md-6">
                                        @if (session('role') && session('role') == 'admin')
                                            <a class="fa fa-edit"
                                                href="{{ route('editCustom', ['id' => $custom->id]) }}"></a>
                                            <a class="fa fa-trash" style="color: red"
                                                href="{{ route('deleteCustom', ['id' => $custom->id]) }}"></a>
                                        @endif
                                        <div class="filtr-item">

                                            <img src="{{ $custom->displayedImage ? $custom->displayedImage->directory : '\images\xoi_ngu_sac.jpg' }}"
                                                alt="portfolio image" />
                                            <div class="item-title">
                                                <a href="{{ route('detailCustom', $custom->id) }}">
                                                    {{ $custom->title }}
                                                </a>
                                            </div><!-- /.item-title -->
                                            <div class="view fa fa-eye">{{ $custom->views }}</div>
                                        </div><!-- /.filtr-item -->
                                    </div><!-- /.col -->
                                @endforeach


                            </div><!-- /.row -->
                            @include('includes.navigation', ['data'=>$customs])
                        </div><!-- /.filtr-container-->
                    </div><!-- /.gallery-content -->
                </div>
                <!--/.galley-box-->
            </div>
            <!--/.gallery-details-->
        </div>
        <!--/.container-->

    </section>
    <!--/.gallery-->

    @include("content.subscribe")
@endsection
