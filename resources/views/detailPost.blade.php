@extends('mainLayout')
@section('content')

    <?php
    $customBanner = [
        'style' => "background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639566351/Hnet.com-image_otudo7.jpg')",
        'banner_name' => 'Diễn đàn thảo luận',
    ];
    ?>
    <!--about-us start -->
    @include("content.home", $customBanner)
    <!--about-us end -->


    <section id="gallery" class="packages">
        <div class="container">
            <div class="gallary-header text-center" id="post{{ $post->id }}">
                <h2> Bài viết của {{ $post->username->username }}
                </h2>
            </div>
            <div>
                <p class="timesNew">
                    {{ $post->content }}
                </p>
                <div class="text-center">
                    @foreach ($post->images as $image)
                        <img src="{{ $image->directory }}" alt="hinh anh" />
                    @endforeach
                </div>
            </div>
            <section id="comment" style="padding-left: 20px"></section>
            <div class="fb-comments" data-href="{{ env('APP_URL') }}detailPost/#post{{ $post->id }}"
                data-width="1000" data-numposts="5"></div>
            <div class="row">
                <div class="about-btn col-sm">
                    <a class="btn about-view packages-btn" href="{{ route('forum') }}" style="color: white">
                        Quay lại
                    </a>
                </div>
            </div>

    </section>

    @include("content.subscribe")

@endsection
