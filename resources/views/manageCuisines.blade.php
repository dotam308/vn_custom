@extends('mainLayout')
@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
            // 'style' =>"background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639566742/Hnet.com-image_1_agb3lp.jpg')",
            'banner_name' => 'Quản lý danh sách ẩm thực'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <div class="gallery-details">
                <div class="main-form">
                    <h3>Quản lí danh sách ẩm thực</h3>
                    <br> <br>

                    <button class="btn btn-info"><a href="{{ route('addCuisine') }}" style="color: white">Thêm</a></button>
                    <table class="table table-striped">
                        <tr class="row timesNew px18">
                            <th class="col-md-1">Code</th>
                            <th class="col-md-2">Tiêu đề</th>
                            <th class="col-md-3">Tóm tắt</th>
                            <th class="col-md-3">Nội dung</th>
                            <th class="col-md-1">Ảnh hiển thị</th>
                            <th class="col-md-2">Action</th>
                        </tr>
                        @foreach ($cuisines as $cuisine)
                            <tr class="row timesNew px18">
                                <td class="col-md-1">{{ $cuisine->id }}</td>
                                <td class="col-md-2">{{ $cuisine->title }}</td>
                                <td class="col-md-3">{{ $cuisine->short_description }}</td>
                                <td class="col-md-3 text-center black">
                                    <p class="black">{{ $cuisine->content }}</p>
                                    @if ($cuisine->images)
                                        @foreach ($cuisine->images as $image)
                                            <img src="{{ $image->directory }}" style="width: 30%"/>
                                            <figcaption class="text-center">{{ $image->title }}</figcaption>
                                            <p class="text-left black">{{ $image->content }}</p>
                                        @endforeach
                                    @endif
                                </td>
                                <td class="col-md-1"><img src="{{ $cuisine->displayedImage ? $cuisine->displayedImage->directory : "" }}" /></td>
                                <td class="col-md-2">
                                    <a class="fa fa-edit" href="{{ route("editCuisine", ["id" => $cuisine->id]) }}"></a>
                                    <a class="fa fa-trash" style="color: red" href="{{ route("deleteCuisine",["id" => $cuisine->id]) }}"></a>
                                    <a class="fa fa-bullhorn" style="color: green"
                                        href="{{ route('broadcastCuisine', $cuisine->id) }}"></a>
                                </td>
                            </tr>

                        @endforeach

                    </table>
                @include('includes.navigation', ['data'=>$cuisines])
                </div>
            </div>
        </div>
    </section>
@endsection
