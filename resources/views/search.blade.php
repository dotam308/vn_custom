@extends('mainLayout')

@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
            'style' =>"background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639571327/Hnet.com-image_1_zoj8zu.png')",
            'banner_name' => 'Công cụ tìm kiếm với Thai Tour'
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <br><br><br><br>
            <div class="gallery-details">
                <div class="gallary-header text-center">
                    <h2>
                        Tìm kiếm
                    </h2>
                    <p>
                        Vui lòng nhập key và loại tương ứng
                    </p>
                </div>


    <section id="search">
        <form action="{{ route('homeSearch') }}" method="get">
            <div class="col-md-4 col-sm-6 form-group" style="margin-left: 10%; width: 20%;">
                <label for="search" class="timesNew">Tìm kiếm theo:</label>
                <select name="searchFor" id="searchFor">
                    <option value="all" class="timesNew px18">Tất cả</option>
                    <option value="custom" class="timesNew px18">Phong tục tập quán</option>
                    <option value="cuisine" class="timesNew px18">Ẩm thực</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group ">
                    <input class="form-control timesNew" placeholder="Nhập từ khoá" id="keywords" name="keywords" type="text">
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="form-group">
                    <input class="form-control timesNew px18" type="submit" value="Tìm kiếm"
                        style="background-color: rgb(255, 94, 0); color: #fff;">
                </div>
            </div>
        </form>
    </section>
            </div>
            <!--/.gallery-details-->
        </div>
        <!--/.container-->

    </section>
    <!--/.gallery-->

    @include("content.subscribe")
@endsection
