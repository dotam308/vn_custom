@extends('mainLayout')

@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
        'style' => "background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1639571882/vid2_e1qjit.png')",
        'banner_name' => 'Hiểu hơn về dân tộc Thái qua các videos',
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <br><br><br><br>
            {{-- <form action="{{route('customSearch')}}" method="get">
                <div class="col-md-4 col-sm-6" style="width:615px">
                    <div class="form-group">
                        <input class="form-control" placeholder="Nhập từ khoá" id="keywords"
                            name="keywords" type="text">
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="form-group">
                        <input class="form-control" type="submit" value="Tìm kiếm" style="background-color: rgb(255, 94, 0); color: #fff;">
                    </div>
                </div>
            </form> --}}
            <br>
            <div class="gallery-details">
                <div class="gallary-header text-center">
                    <h2>
                        Một số thước phim sưu tập liên quan
                    </h2>
                    <p>
                        Cười là loại mĩ phẩm rẻ nhất, vận động là loại y dược rẻ nhất, chào hỏi là loại chi phí giao tiếp rẻ
                        nhất
                    </p>
                </div>
                <table>
                            @foreach ($videos as $video)
                            <tr class="row">
                                <td class="col-md-6">
                                <a href="{{ route('detailVideo', ['id' => $video->id]) }}"><img width="560" height="315"
                                        src="{{ $video->image_url }}" /></a>
                                    </td>
                                <td>
                                    <h2 class="timesNew">{{ $video->short_description }}</h2>
                                    <p class="timesNew">{{ $video->content }}</p>
                                </td>
                            </tr>
                            @endforeach

                </table>

            </div>
            <!--/.gallery-details-->
            @include('includes.navigation', ['data'=>$videos])
        </div>
        <!--/.container-->

    </section>
    <!--/.gallery-->

    @include("content.subscribe")
@endsection
