<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    {{-- <h1>{{ $details['title'] }}</h1> --}}
    <p>Xin chào, </p>
    <p>Chúng tôi gửi mail từ hệ thống website ThaiTour mà bạn đã đăng ký.</p>
    <p>ThaiTour vừa có bài viết rất hay và thú vị.</p>
    <p><a href={{ $details["link"] }}>{{ $details['body'] }}</a></p>
    <p>Cảm ơn bạn đã đăng ký nhận thông báo</p>
    <p>Thân ái,</p>
    <p>ThaiTour</p>
</body>
</html>
