<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    {{-- <h1>{{ $details['title'] }}</h1> --}}
    <div class="timesNew">
        <p>Xin chào, </p>
        <p>Chúng tôi gửi mail từ hệ thống website ThaiTour mà bạn đã phản hồi.</p>
        <p>ThaiTour trả lời phản hồi của bạn như sau.</p>
        <p>{{ $details['body'] }}</p>
        <p>Cảm ơn bạn đã gửi phản hồi</p>
        <p>Thân ái,</p>
        <p>ThaiTour</p>
    </div>
</body>
</html>
