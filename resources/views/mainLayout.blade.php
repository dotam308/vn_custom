<!doctype html>
<html class="no-js" lang="en">

<head>
    @include("includes.head")
    @yield('script')
    @yield('style')
    @yield('head')
</head>

<body>
    <div id="fb-root"></div>

    <!-- Your Chat Plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
        var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "103466595534802");
        chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                xfbml: true,
                version: 'v12.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v12.0&appId=157330463046823&autoLogAppEvents=1"
        nonce="2UKGW9bN"></script>
    <!-- main-menu Start -->
    <header class="top-area">
        @include("includes.header")
    </header><!-- /.top-area-->
    <!-- main-menu End -->

    @yield('content')

    <footer class="footer-copyright">
        @include("includes.footer")
    </footer><!-- /.footer-copyright-->
    <!-- footer-copyright end -->

    @include("includes.script")
    @include('sweetalert::alert')
    <!-- Messenger Chat Plugin Code -->

</body>

</html>
