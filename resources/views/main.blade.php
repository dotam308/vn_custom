@extends('mainLayout')
@section('style')
    <style type="text/css">
        .gallary-header h2 {
            color: rgb(99, 87, 128);
        }

        .home1-testm-txt p {
            color: #00D8FF;
        }

        .home1-testm-txt span i {
            color: #00D8FF;
        }

    </style>
@endsection
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->
    <br><br><br><br>
    <section id="search">
        <form action="{{ route('homeSearch') }}" method="get">
            <div class="col-md-4 col-sm-6 form-group" style="margin-left: 10%; width: 20%;">
                <label for="search" class="timesNew">Tìm kiếm theo:</label>
                <select name="searchFor" id="searchFor">
                    <option value="all" class="timesNew px18">Tất cả</option>
                    <option value="custom" class="timesNew px18">Phong tục tập quán</option>
                    <option value="cuisine" class="timesNew px18">Ẩm thực</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group ">
                    <input class="form-control timesNew" placeholder="Nhập từ khoá" id="keywords" name="keywords"
                        type="text">
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="form-group">
                    <input class="form-control timesNew px18" type="submit" value="Tìm kiếm"
                        style="background-color: rgb(255, 94, 0); color: #fff;">
                </div>
            </div>
        </form>
    </section>
    <br>
    @if (!request()->get('searchFor'))
        @include("content.introduction")
    @endif
    @if (!request()->get('searchFor') || request()->get('searchFor') == 'custom' || request()->get('searchFor') == 'all')
        <!--galley start-->
        @include("content.custom", $customs)
        <!--gallery end-->
    @endif
    @if (!request()->get('searchFor') || request()->get('searchFor') == 'cuisine' || request()->get('searchFor') == 'all')
        <!--packages start-->
        @include("content.cuisine", $cuisines)
        <!--packages end-->
    @endif

    @if (!request()->get('searchFor'))
        @include("content.videos")
    @endif

    @if (!request()->get('searchFor'))
        @include("content.games")
    @endif

    @if (!request()->get('searchFor'))
        @include("content.forum")
    @endif

    @include("content.subscribe")

@endsection
