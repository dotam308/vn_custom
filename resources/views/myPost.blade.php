@extends('mainLayout')

@section('content')

    <!--about-us start -->
    <?php
    $customBanner = [
        'style' => "background: url('https://res.cloudinary.com/dbiexlh94/image/upload/v1640088096/Hnet.com-image_4_qowei4.jpg')",
        'banner_name' => 'Bài viết của tôi',
    ];
    ?>
    @include("content.home", $customBanner)
    <!--about-us end -->
    <section id="gallery" class="gallery">
        <div class="container">
            <br><br>
            <div>
                <form action="{{ route('myPostSearch') }}" method="get">
                    <div class="col-md-4 col-sm-6" style="width:615px">
                        <div class="form-group">
                            <input class="form-control" placeholder="Nhập từ khoá" id="keywords" name="keywords"
                                type="text">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type="submit" value="Tìm kiếm"
                                style="background-color: rgb(255, 94, 0); color: #fff;">
                        </div>
                    </div>
                </form>
            </div>

            <div class="gallery-details">
                <button class="btn btn-info"><a href="{{ route('addNewPost') }}" style="color: white">Thêm bài viết
                        mới</a></button>
                <div class="gallary-header text-center">
                    <h2>
                        Bài viết của tôi
                    </h2>
                    <p>
                        {{-- Cười là loại mĩ phẩm rẻ nhất, vận động là loại y dược rẻ nhất, chào hỏi là loại chi phí giao tiếp rẻ
                        nhất --}}
                    </p>
                </div>
                <!--/.gallery-header-->
                <div class="gallery-box">
                    <div class="gallery-content">
                        <div class="filtr-container">
                            <div class="row">
                                @foreach ($posts as $post)
                                    @php
                                        $t = $post->created_at;
                                        date_modify($t, '+7 hours');
                                        $time = date_format($t, 'H:m d/m/Y');
                                    @endphp
                                    <div style="background-color: #ccc; margin: 30px;" id="post{{ $post->id }}">
                                        <h4 class="timesNew"> từ:
                                            {{ $post->username->username }}
                                            <div style="float: right;">
                                                <a class="fa fa-edit" href="{{ route("editPost", ["id" => $post->id]) }}"></a>
                                                <a class="fa fa-trash" style="color: red" href="{{ route("deletePost", ["id" => $post->id]) }}"></a>
                                            </div>
                                        </h4>
                                        <p class="timesNew" style="font-size: 14px;">Ngày đăng {{ $time }}</p>
                                        
                                        <br>
                                        <p class="timesNew" style="color: #000; text-overflow: ellipsis;
                                                                overflow: hidden;
                                                                white-space: nowrap;">
                                            {{ $post->content }}
                                        </p>
                                        <a href="{{ route('detailPost', ['id' => $post->id]) }}">Xem thêm</a>
                                        <div class="row text-center">
                                            <?php
                                            $count = 0;
                                            ?>
                                            @foreach ($post->images as $image)
                                                {{-- <br><br> --}}
                                                <img src="{{ $image->directory }}" alt="hinh anh" height="200px"
                                                    weight="200px" id="{{ $image->id . $count++ }}" />
                                                @if ($count > 2)
                                                <a href="{{ route('detailPost', ['id' => $post->id]) }}"><i class="fa fa-plus"></i></a>
                                                    @break
                                                @endif
                                            @endforeach
                            </div>
                            <br><br><br>
                        </div>

                        <section id="comment" style="padding-left: 20px"></section>
                        <div class="fb-comments" data-href="{{ env('APP_URL') }}detailPost/#post{{ $post->id }}"
                            data-width="1000" data-numposts="5"></div>
                        @endforeach

                    </div><!-- /.row -->
                    {{-- @include('includes.navigation', ['data'=>$customs]) --}}
                </div><!-- /.filtr-container-->
            </div><!-- /.gallery-content -->
        </div>
        <!--/.galley-box-->
        </div>
        <!--/.gallery-details-->
        </div>
        <!--/.container-->
        @include('includes.navigation', ['data'=>$posts])
    </section>
    <!--/.gallery-->

    @include("content.subscribe")
@endsection

<style>
    .column {
        float: left;
        width: 30%;
        padding: 5px;
        margin: 15px;
    }

    /* Clear floats after image containers */
    .row::after {
        content: "";
        clear: both;
        display: table;
    }

</style>
