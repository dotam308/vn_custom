@extends('mainLayout')
@section('content')

    <!--about-us start -->
    @include("content.home")
    <!--about-us end -->


    <section id="gallery" class="packages">
        <div class="container">
            <div class="gallary-header text-center">
                <h2>
                    {{ $cuisine->title }}
                </h2>
            </div>
            <div>
                <p class="timesNew">
                    {{ $cuisine->content }}
                </p>
                @forelse ($cuisine->images as $image)
                    <div class="text-center">
                        <img src="{{ $image->directory }}" alt="hinh anh" />
                    </div>
                    <figcaption class="text-center">{{ $image->title }}</figcaption>
                    <p class="timesNew">{{ $image->content }}</p>
                    <br>
                    <br>
                @empty

                @endforelse
            </div>
            <div class="row">
                <div class="about-btn col-sm">
                    <a class="btn about-view packages-btn" href="{{ route('allCuisines') }}" style="color: white">
                        Quay lại
                    </a>
                </div>
            </div>

            <section id="comment"></section>
            <div class="fb-comments" data-href="{{ env('APP_URL') }}detail-cuisine/{{ $cuisine->id }}#comment"
            data-width="1000" data-numposts="5"></div>

            <br><br><br>
            <h2>Có thể bạn quan tâm</h2>
            <!--/.gallery-header-->
            <div class="gallery-box">
                <div class="gallery-content">
                    <div class="filtr-container">
                        <div class="row">
                            @foreach ($relatedCuisines as $cuisine)
                                <div class="col-md-6">
                                    <div class="filtr-item">

                                        <img src="{{ $cuisine->displayedImage ? $cuisine->displayedImage->directory : '\images\xoi_ngu_sac.jpg' }}"
                                            alt="portfolio image" />
                                        <div class="item-title">
                                            <a href="{{ route('detailCuisine', $cuisine->id) }}">
                                                {{ $cuisine->title }}
                                            </a>
                                        </div><!-- /.item-title -->
                                    </div><!-- /.filtr-item -->
                                </div><!-- /.col -->
                            @endforeach


                        </div><!-- /.row -->
                    </div><!-- /.filtr-container-->
                </div><!-- /.gallery-content -->
            </div>
    </section>

    @include("content.subscribe")

@endsection
