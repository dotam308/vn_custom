<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-1">
                <div class="logo">
                    <a href="{{ route('home') }}">
                        Thai<span>Tour</span>
                    </a>
                </div><!-- /.logo-->
            </div><!-- /.col-->
            <div class="col-sm-11">
                <div class="main-menu">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button><!-- / button-->
                    </div><!-- /.navbar-header-->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            @if (session("role") && session("role") == "admin")
                                <li class="dropdown">
                                    <a>Quản lý</a>
                                    <div class="dropdown-content">
                                        <a href="{{ route("manageCustoms") }}" style="color: black" class="dropdown-item dropdown-hover">Phong tục tập quán</a>
                                        <a href="{{ route("manageCuisines") }}" style="color: black" class="dropdown-item dropdown-hover">Ẩm thực</a>
                                        <a href="{{ route("viewFeedback") }}" style="color: black" class="dropdown-item dropdown-hover">Phản hồi</a>
                                    </div>
                                </li>
                            @endif
                            <li><a href="{{ route("home") }}">Trang chủ</a></li>
                            <li><a href="{{ route("introduction") }}">Giới thiệu</a></li>

                            <li class="dropdown">
                                <a>Tài nguyên</a>
                                <div class="dropdown-content">
                                    <div>
                                        <a href="{{ route("allCustoms") }}"  class="dropdown-item dropdown-hover">Phong tục tập quán</a>
                                    </div>
                                    <div>
                                        <a href="{{ route("allCuisines") }}" class="dropdown-item dropdown-hover">Ẩm thực</a>
                                    </div>
                                    <div>
                                        <a href="{{ route("allVideos") }}" class="dropdown-item dropdown-hover">Video</a>
                                    </div>
                                </div>
                            </li>
                            <li><a href="{{ route("games") }}">Trò chơi</a></li>
                            <li><a href="{{ route("forum") }}">Diễn đàn</a></li>
                            <li><a href="{{ route("search") }}">Tìm kiếm</a></li>
                            <li><a href="{{ route("feedback") }}">Phản hồi</a></li>
                            <li class="dropdown">
                                <a>Cá nhân</a>
                                <div class="dropdown-content">
                                    <div>
                                        <a href="{{ route("me") }}"  class="dropdown-item dropdown-hover">Tài khoản</a>
                                    </div>
                                    <div>
                                        <a href="{{ route("myPosts") }}" class="dropdown-item dropdown-hover">Bài viết</a>
                                    </div>
                                </div>
                            </li>
                            <li style="margin-bottom: 20px">
                                @if (session("login") && session("login") == true)
                                    <a href="{{ route("logout") }}"><button class="book-btn">Đăng xuất</button></a>
                                @else
                                    <a href="{{ route("login") }}"><button class="book-btn">Đăng nhập</button></a>
                                @endif
                            </li>
                            <!--/.project-btn-->
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.main-menu-->
            </div><!-- /.col-->
        </div><!-- /.row -->
        <div class="home-border"></div><!-- /.home-border-->
    </div><!-- /.container-->
</div><!-- /.header-area -->
