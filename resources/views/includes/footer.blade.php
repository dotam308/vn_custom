<div class="container">
    <div class="footer-content">
        <div class="row">

            <div class="col-sm-3">
                <div class="single-footer-item">
                    <div class="footer-logo">
                        <a href="index.html">
                            Thai<span>Tour</span>
                        </a>
                        <p>
                            Kênh thông tin về dân tộc Thái
                        </p>
                    </div>
                </div>
                <!--/.single-footer-item-->
            </div>
            <!--/.col-->

            <div class="col-sm-3">
                <div class="single-footer-item">
                    <h2>link</h2>
                    <div class="single-footer-txt">
                        <p><a href="{{ route("home") }}">Trang chủ</a></p>
                        <p><a href="{{ route("introduction") }}">Giới thiệu</a></p>
                        <p><a href="{{ route("allCustoms") }}">Phong tục tập quán</a></p>
                        <p><a href="{{ route("allCuisines") }}">Ẩm thực</a></p>
                        <p>
                            @if(session("role") && session("role") != "")
                            <a href="{{ route("logout") }}">Đăng xuất</a>
                            @else
                            <a href="{{ route("login") }}">Đăng ký</a>
                            @endif
                        </p>
                    </div>
                    <!--/.single-footer-txt-->
                </div>
                <!--/.single-footer-item-->

            </div>
            <!--/.col-->

            {{-- <div class="col-sm-3">
                <div class="single-footer-item">
                    <h2>popular destination</h2>
                    <div class="single-footer-txt">
                        <p><a href="#">china</a></p>
                        <p><a href="#">venezuela</a></p>
                        <p><a href="#">brazil</a></p>
                        <p><a href="#">australia</a></p>
                        <p><a href="#">london</a></p>
                    </div>
                    <!--/.single-footer-txt-->
                </div>
                <!--/.single-footer-item-->
            </div> --}}
            <!--/.col-->

            <div class="col-sm-3">
                <div class="single-footer-item text-center">
                    <h2 class="text-left">Liên hệ</h2>
                    <div class="single-footer-txt text-left">
                        <p>0967917307</p>
                        <p class="foot-email"><a href="#" >mythaitour.app@gmail.com</a></p>
                        <p>136 Xuân Thuỷ, Cầu Giấy</p>
                        <p>Hà Nội, Việt Nam</p>
                    </div>
                    <!--/.single-footer-txt-->
                </div>
                <!--/.single-footer-item-->
            </div>
            <!--/.col-->

        </div>
        <!--/.row-->

    </div>
    <!--/.footer-content-->
    <hr>
    <div class="foot-icons ">
        <ul class="footer-social-links list-inline list-unstyled">
            <li><a href="#" target="_blank" class="foot-icon-bg-1"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" target="_blank" class="foot-icon-bg-2"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" target="_blank" class="foot-icon-bg-3"><i class="fa fa-instagram"></i></a></li>
        </ul>
        <p>&copy; 2017 <a href="https://www.themesine.com">ThemeSINE</a>. All Right Reserved</p>

    </div>
    <!--/.foot-icons-->
    <div id="scroll-Top">
        <i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top"
            title="" data-original-title="Back to Top" aria-hidden="true"></i>
    </div>
    <!--/.scroll-Top-->
</div><!-- /.container-->
