@php
    $c = $data->currentPage();
    $r = $data->lastPage();
    $lastPage = $data->lastPage();
    $l = 1;
    if ($lastPage > 9) {
        if ($c > 5) {
            if ($c + 5 < $r) {
                $l = ($c-4);
                $r = ($c + 4);
            } else {
                $l = $r-9;
            }
        } elseif ($r > 9) {
            $r = 9;
        }
    }
@endphp

<nav class="navigation pagination" style="width: 100%; text-align: center;" >
    <div class="nav-links">
        @if($c > 1)
            <a class="page-numbers" href="{{ $data->url(1) }}"><i class="fa fa-angle-double-left"></i></a>
            <a class="page-numbers" href="{{ $data->url($c-1) }}"><i class="fa fa-arrow-left"></i></a>
        @endif
        @for($i = $l; $i <= $r; $i++)
            @if($i != $c)
                <a class="page-numbers" href="{{ $data->url($i) }}">{{ $i }}</a>
            @else
                <span class="page-numbers current">{{ $i }}</span>
            @endif
        @endfor
        @if($c < $r)
            <a class="page-numbers" href="{{ $data->url($c+1) }}"><i class="fa fa-arrow-right"></i></a>
            <a class="page-numbers" href="{{ $data->url($lastPage) }}"><i class="fa fa-angle-double-right"></i></a>
        @endif
    </div>
</nav>

<style>
    .page-numbers {
        border-color: #f4f4f4;
        border-width: 3px;
        border-style: solid;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        color: #6b6b6b;
        display: inline-block;
        font-weight: 400;
        line-height: 14px;
        margin: 0 5px;
        padding: 8px 0px;
        text-align: center;
        width: 36px;
    }

    .page-numbers.current {
    color: #fff;
    background-color: #ff7236;
    border-color: #ff7236;
    }
</style>