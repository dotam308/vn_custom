<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\CuisineController;
use App\Http\Controllers\CustomController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\FeedbackController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [HomeController::class, "index"])->name("home");
Route::get('/login', [UserController::class, "loginPage"])->name("login");
Route::post('/login', [UserController::class, "authenticate"])->name("authenticate");

Route::get('/logout', [UserController::class, "logout"])->name("logout");



Route::post('/register', [UserController::class, "register"])->name("register");

Route::get('/cuisine', [CuisineController::class, "index"])->name('allCuisines');
Route::get('/detail-cuisine/{id}', [CuisineController::class, "show"])->name('detailCuisine');

Route::get('/custom', [CustomController::class, "index"])->name('allCustoms');
Route::get('/detail-custom/{id}', [CustomController::class, "show"])->name('detailCustom');


Route::get('/introduction', function () {
    return view("introduction");
})->name('introduction');

//search
Route::get('/SearchCustom', [SearchController::class, "customSearch"])->name('customSearch');
Route::get('/SearchCuisine', [SearchController::class, "cuisineSearch"])->name('cuisineSearch');
Route::get('/SearchHome', [SearchController::class, "homeSearch"])->name('homeSearch');
Route::get('/SearchPost', [SearchController::class, "forumSearch"])->name('forumSearch');
Route::get('/SearchMyPost', [SearchController::class, "myPostSearch"])->name('myPostSearch');

Route::post("/subscribe", [HomeController::class, "subscribe"])->name('subscribe');

Route::get('/videos', [VideoController::class, "index"])->name('allVideos');
Route::get('/detail-video/{id}', [VideoController::class, "show"])->name('detailVideo');
Route::get('/search', function () {
    return view("search");
})->name('search');

//forum
Route::get('/forum', [ForumController::class, "viewAllPost"])->name('forum');
Route::get('/detailPost/{id}', [ForumController::class, "showDetail"])->name('detailPost');

Route::get('/all-games', [GameController::class, "index"])->name('games');

// Route::get('/mygames', function() {return "yes";})->name('games');
Route::get('/startGame/{id}', [GameController::class, "startGame"])->name('startGame');

Route::get('/sendFeedback', function () {
    return view("feedback");
})->name('feedback');
Route::post('/sendFeedback', [FeedbackController::class, "handleSendFeedback"])->name('Sendfeedback');


Route::group(['middleware' => 'authCustom'], function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::get('/manage-customs', [HomeController::class, "manageCustoms"])->name("manageCustoms");
        Route::get('/delete-custom', [CustomController::class, "deleteCustom"])->name("deleteCustom");
        Route::get('/edit-custom', [CustomController::class, "editCustom"])->name("editCustom");
        Route::post('/edit-detail-custom', [CustomController::class, "editDetailCustom"])->name("editDetailCustom");
        Route::get('/add-custom', [CustomController::class, "addCustom"])->name("addCustom");
        Route::post('/add-custom', [CustomController::class, "addCustomPost"]);
        Route::delete('/delete-custom-image/{imageId}', [CustomController::class, "deleteCustomImage"]);

        Route::get('/manage-cuisines', [HomeController::class, "manageCuisines"])->name("manageCuisines");
        Route::get('/delete-cuisine', [CuisineController::class, "deleteCuisine"])->name("deleteCuisine");
        Route::get('/edit-cuisine', [CuisineController::class, "editCuisine"])->name("editCuisine");
        Route::post('/edit-detail-cuisine', [CuisineController::class, "editDetailCuisine"])->name("editDetailCuisine");
        Route::get('/add-cuisine', [CuisineController::class, "addCuisine"])->name("addCuisine");
        Route::post('/add-cuisine', [CuisineController::class, "addCuisinePost"]);
        Route::delete('/delete-cuisine-image/{imageId}', [CuisineController::class, "deleteCuisineImage"]);


        Route::get('/broadcast-custom/{id}', [CustomController::class, "broadcastCustom"])->name('broadcastCustom');
        Route::get('/broadcast-cuisine/{id}', [CuisineController::class, "broadcastCuisine"])->name('broadcastCuisine');


        Route::get('/viewFeedback', [FeedbackController::class, "viewFeedback"])->name('viewFeedback');
        Route::get('/handleFeedback/{id}', [FeedbackController::class, "handleFeedback"])->name('handleFeedback');
        Route::post('/handleFeedback/{id}', [FeedbackController::class, "actionForFeedback"])->name('actionForFeedback');
    });
    Route::get('/me', [AccountController::class, "getMyProfile"])->name('me');
    Route::post('/change-password', [AccountController::class, "changePassword"])->name('changePassword');
    Route::get('/my-posts', [AccountController::class, "getMyPosts"])->name('myPosts');
    Route::get('/edit-post', [ForumController::class, "editPost"])->name("editPost");
    Route::post('/edit-post', [ForumController::class, "handleEditPost"])->name("handleEditPost");
    Route::get('/delete-post', [ForumController::class, "deletePost"])->name("deletePost");
    Route::get('/delete-post-forum', [ForumController::class, "deletePostForum"])->name("deletePostForum");
    Route::delete('/delete-post-image/{imageId}', [ForumController::class, "deletePostImage"]);

    Route::get('/addNewPost', function () {
        return view("addPost");
    })->name('addNewPost');
    Route::post('/addNewPost', [ForumController::class, "handleAddPost"]);
});
