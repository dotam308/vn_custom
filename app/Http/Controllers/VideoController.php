<?php

namespace App\Http\Controllers;

use App\Models\Video;

class VideoController extends Controller
{
    public function index() {
        $videos = Video::paginate(6);
        return view('videos', compact('videos'));
    }

    public function show($id) {
        $video = Video::where('id', $id)->first();
        $relatedVideos = Video::inRandomOrder()->paginate(6);
        return view('detailVideo', compact('video', 'relatedVideos'));
    }

}
