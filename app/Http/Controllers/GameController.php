<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index()
    {
        return view("games");
    }

    public function startGame($id)
    {
        if ($id == 1)
            return view("games.quiz-js.index");
        return view("games.tower.index");
    }
}
