<?php

namespace App\Http\Controllers;

use App\Models\Cuisine;
use App\Models\Custom;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index() {
        $cuisines = Cuisine::orderBy("views", "desc")->limit(6)->get();
        $customs = Custom::orderBy("views", "desc")->limit(7)->get();
        return view("main", compact('cuisines', 'customs'));
    }

    public function manageCustoms() {
        $customs = Custom::paginate(6);
        foreach ($customs as $custom) {
            $custom["images"] = DB::table("custom_image")
                                ->leftJoin("images", "images.id", "=", "custom_image.image_id")
                                ->where("custom_image.custom_id", "=", $custom->id)
                                ->get();
        }
        return view("manageCustoms", compact('customs'));
    }

    public function manageCuisines() {
        $cuisines = Cuisine::paginate(6);
        foreach ($cuisines as $cuisine) {
            $cuisine["images"] = DB::table("cuisine_image")
                                ->leftJoin("images", "images.id", "=", "cuisine_image.image_id")
                                ->where("cuisine_image.cuisine_id", "=", $cuisine->id)
                                ->get();
        }
        return view("manageCuisines", compact('cuisines'));
    }

    public function subscribe(Request $request) {
        $had = Subscriber::where("email", $request->email)->get();
        if (count($had) == 0) {
            Subscriber::create(["email" => $request->email]);
            toast('Đăng ký thành công','success');
        } else {
            toast('Đã đăng ký','success');
        }
        return redirect()->back();
    }
}
