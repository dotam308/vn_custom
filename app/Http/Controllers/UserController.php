<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;


class UserController extends Controller
{
    public function loginPage()
    {
        return view("login");
    }

    public function authenticate(Request $request)
    {
        $user = User::where("username", $request->username)->where("password", md5($request->password))->first();
        if ($user) {
            toast('Đăng nhập thành công','success');
            session(["login" => true, "role" => $user->role, "userId" => $user->id]);
            return redirect()->route("home");
        }
        toast('Tài khoản hoặc mật khẩu không chính xác','warning');
        return redirect()->route("login");
    }

    public function logout() {
        Session::flush();
        toast('Đăng xuất thành công','success');
        return redirect()->route("home");
    }

    public function register(Request $request) {
        if (!$this->getUserByUserName($request->username)) {
            if ($request->password != $request->password_retype) {
                toast('Nhập lại mật khẩu không đúng','info');
                return redirect()->route("login");
            }
            toast('Đăng ký thành công','success');
            $user = User::create(["username" => $request->username, "password" => md5($request->password), "role" => "guest"]);
            session(["login" => true, "role" => $user->role, "userId" => $user->id]);

            return redirect()->route("home");
        }
        alert()->warning('Cảnh báo','Tài khoản đã tồn tại.');
        return redirect()->route("login");
    }

    public function getUserByUserName($userName) {
        return User::where(["username" => $userName])->first();
    }
}
