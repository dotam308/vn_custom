<?php

namespace App\Http\Controllers;

use App\Models\Custom;
use App\Models\Cuisine;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class SearchController extends Controller
{
    public function customSearch() {
        $search = $_GET['keywords'];
        $customs = Custom::where('title', 'like', "%{$search}%")
        ->orWhere('content', 'like', "%{$search}%")
        ->orWhere('short_description', 'like', "%{$search}%")->paginate(6);
        // $customs = Custom::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE)")->paginate(6);
        return view('custom', compact('customs'));
    }

    public function cuisineSearch() {
        $search = $_GET['keywords'];
        $cuisines = Cuisine::where('title', 'like', "%{$search}%")
        ->orWhere('content', 'like', "%{$search}%")
        ->orWhere('short_description', 'like', "%{$search}%")->paginate(6);
        // $cuisine = Cuisine::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE)")->paginate(6);
        return view('cuisine', compact('cuisines'));
    }

    public function homeSearch() {
        $search = $_GET['keywords'];
        $searchFor = $_GET['searchFor'];
        if ($searchFor == 'all') {
            $cuisines = Cuisine::where('title', 'like', "%{$search}%")
            ->orWhere('content', 'like', "%{$search}%")
            ->orWhere('short_description', 'like', "%{$search}%")->get();
            // $cuisines = Cuisine::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE) OR (title like '%{$search}%') OR (content like '%{$search}%') OR (short_description like '%{$search}%');")->get();
            $customs = Custom::where('title', 'like', "%{$search}%")
            ->orWhere('content', 'like', "%{$search}%")
            ->orWhere('short_description', 'like', "%{$search}%")->get();
            // $customs = Custom::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE) OR (title like '%{$search}%') OR (content like '%{$search}%') OR (short_description like '%{$search}%')")->get();
        } elseif ($searchFor == 'custom') {
            $customs = Custom::where('title', 'like', "%{$search}%")
            ->orWhere('content', 'like', "%{$search}%")
            ->orWhere('short_description', 'like', "%{$search}%")->get();
            // $customs = Custom::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE)")->get();
            $cuisines = Cuisine::take(0)->get();
        } else {
            $cuisines = Cuisine::where('title', 'like', "%{$search}%")
            ->orWhere('content', 'like', "%{$search}%")
            ->orWhere('short_description', 'like', "%{$search}%")->get();
            // $cuisines = Cuisine::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE)")->get();
            $customs = Custom::take(0)->get();
        }
        return view("main", compact('cuisines', 'customs'));
    }

    public function forumSearch() {
        $key = $_GET['keywords'];

        $posts = Post::where('content', 'like', "%{$key}%")->paginate(10);
        foreach($posts as $post) {
            $post["images"] = DB::table("post_image")
            ->leftJoin("images", "images.id", "=", "post_image.image_id")
            ->where("post_image.post_id", "=", $post->id)
            ->get();
            $post["username"] = User::where("id", "=", $post->user_id)->get('username')->first();
        }


        // $customs = Custom::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE)")->paginate(6);
        return view('forum', compact('posts'));
    }

    public function myPostSearch() {
        $key = $_GET['keywords'];

        $posts = Post::where('content', 'like', "%{$key}%")->where('user_id', session("userId"))->paginate(10);
        foreach($posts as $post) {
            $post["images"] = DB::table("post_image")
            ->leftJoin("images", "images.id", "=", "post_image.image_id")
            ->where("post_image.post_id", "=", $post->id)
            ->get();
            $post["username"] = User::where("id", "=", $post->user_id)->get('username')->first();
        }


        // $customs = Custom::whereRaw("MATCH(title, content, short_description) AGAINST('{$search}' IN BOOLEAN MODE)")->paginate(6);
        return view('myPost', compact('posts'));
    }
}
