<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\Cuisine;
use App\Models\CuisineImage;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CuisineController extends Controller
{
    public function index()
    {
        $cuisines = Cuisine::paginate(6);
        return view('cuisine', compact('cuisines'));
    }

    public function show($id)
    {
        $cuisine = Cuisine::where("id", $id)->first();
        $cuisine->views++;
        $cuisine->save();
        $cuisine["images"] = DB::table("cuisine_image")
            ->leftJoin("images", "images.id", "=", "cuisine_image.image_id")
            ->where("cuisine_image.cuisine_id", "=", $cuisine->id)
            ->get();
        $relatedCuisines = Cuisine::inRandomOrder()->paginate(6);
        return view('detailCuisine', compact('cuisine', 'relatedCuisines'));
    }

    public function deleteCuisine(Request $request)
    {
        $cuisine = Cuisine::find($request->id)->delete();
        alert()->success('Thông báo:', 'Xóa ẩm thực thành công.');
        return redirect()->back();
    }

    public function editCuisine(Request $request)
    {
        $cuisine = Cuisine::where('id', $request->id)->first();
        $cuisine["images"] = DB::table("cuisine_image")
            ->leftJoin("images", "images.id", "=", "cuisine_image.image_id")
            ->where("cuisine_image.cuisine_id", "=", $cuisine->id)
            ->get();
        return view("editCuisine", compact('cuisine'));
    }

    public function editDetailCuisine(Request $request)
    {
        toast('Sửa ẩm thực thành công', 'success');
        $cuisine = Cuisine::where('id', $request->id)->first();
        $cuisine->update($request->all());

        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->file("image")) {
            $result = $request->file("image")->storeOnCloudinary();
            $directory = $result->getPath();
            $uploadImage = Image::create(["directory" => $directory]);
            $cuisine->update(["displayed_image_id" => $uploadImage->id]);
        }

        $imagesOfCuisine = CuisineImage::where("cuisine_id", $cuisine->id)->get();
        if ($request->oldTitles) {
            foreach ($request->oldTitles as $key => $title) {
                $newImage = Image::where(["id" => $imagesOfCuisine[$key]->image_id]);
                if (!empty($request->file("oldImages")[$key])) {
                    $detailImage = $request->file("oldImages")[$key]->storeOnCloudinary();
                    $dir = $detailImage->getPath();
                    $newImage->update(["directory" => $dir]);
                }
                $newImage->update(
                    [
                        "title" => $title ?? "",
                        "content" => $request->oldContents[$key] ?? ""
                    ]
                );
            }
        }
        if ($request->file("images")) {
            foreach ($request->file("images") as $key => $image) {
                $detailImage = $image->storeOnCloudinary();
                $dir = $detailImage->getPath();
                $uploadAddImage = Image::create(
                    [
                        "directory" => $dir,
                        "title" => $request->titles[$key] ?? "",
                        "content" => $request->contents[$key] ?? ""
                    ]
                );
                CuisineImage::create(
                    [
                        "cuisine_id" => $cuisine->id,
                        "image_id" => $uploadAddImage->id
                    ]
                );
            }
        }
        // return redirect()->back();
        return redirect(route('manageCuisines'));
    }

    public function addCuisine()
    {
        return view("addCuisine");
    }

    public function addCuisinePost(Request $request)
    {
        toast('Thêm ẩm thực thành công', 'success');
        $cuisine = Cuisine::create($request->all());
        if ($request->mail && $request->mail == "yes") Helpers::sendMailToSubscriber($cuisine->id, $cuisine->title, "cuisine");
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->file("image")) {
            $result = $request->file("image")->storeOnCloudinary();
            $directory = $result->getPath();
            $uploadImage = Image::create(["directory" => $directory]);
            $cuisine->update(["displayed_image_id" => $uploadImage->id]);
        }
        if ($request->file("images")) {
            foreach ($request->file("images") as $key => $image) {
                $detailImage = $image->storeOnCloudinary();
                $dir = $detailImage->getPath();
                $uploadAddImage = Image::create(
                    [
                        "directory" => $dir,
                        "title" => $request->titles[$key] ?? "",
                        "content" => $request->contents[$key] ?? ""
                    ]
                );
                CuisineImage::create(
                    [
                        "cuisine_id" => $cuisine->id,
                        "image_id" => $uploadAddImage->id
                    ]
                );
            }
        }
        if ($request->addOne) {
            return redirect()->route("manageCuisines");
        }
        return redirect()->back();
    }

    public function deleteCuisineImage($imageId)
    {
        $cuisineImage = CuisineImage::where(["image_id" => $imageId])->first();
        if ($cuisineImage) $cuisineImage->delete();
        $cuisine = Cuisine::where(["displayed_image_id" => $imageId])->first();
        if ($cuisine) $cuisine->update(["displayed_image_id" => NULL]);
        $image = Image::where(["id" => $imageId])->first();
        if ($image) $image->delete();
        return response()->json(["message" => "Delete successfully", "status" => 200]);
    }

    public function broadcastCuisine($id) {
        Helpers::sendMailToSubscriber($id, null, "cuisine");
        return redirect()->route("manageCuisines");
    }
}
