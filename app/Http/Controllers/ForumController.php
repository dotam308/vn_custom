<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use App\Models\Post;
use App\Models\PostImage;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ForumController extends Controller
{
    public function viewAllPost() {
        $posts = Post::orderBy('id', 'DESC')->paginate(10);
        foreach($posts as $post) {
            $post["images"] = DB::table("post_image")
            ->leftJoin("images", "images.id", "=", "post_image.image_id")
            ->where("post_image.post_id", "=", $post->id)
            ->get();
            $post["user"] = User::where("id", "=", $post->user_id)->get()->first();
        }
        return view('forum', compact('posts'));
    }

    public function handleAddPost(Request $request) {
        //if not signIn
        if (session('userId') == null) {
            alert()->warning('Cảnh báo','Bạn cần đăng nhập trước')->width('500px');
            return redirect()->back();
        }

        $post = Post::create(
            [
                "user_id" => session('userId'),
                "content" => $request->content
            ]
            );

        if ($request->file("images")) {
            foreach ($request->file("images") as $key => $image) {
                $detailImage = $image->storeOnCloudinary();
                $dir = $detailImage->getPath();
                $uploadAddImage = Image::create(
                    [
                        "directory" => $dir,
                        "title" => " ",
                        "content" => " "
                    ]
                );
                PostImage::create(
                    [
                        "post_id" => $post->id,
                        "image_id" => $uploadAddImage->id
                    ]
                );
            }
        }
        return redirect()->route("forum");
    }

    public function showDetail($id) {
        $post = Post::where("id", $id)->first();
        $post["images"] = DB::table("post_image")
        ->leftJoin("images", "images.id", "=", "post_image.image_id")
        ->where("post_image.post_id", "=", $post->id)
        ->get();
        $post["username"] = User::where("id", "=", $post->user_id)->get('username')->first();
        return view('detailPost', compact('post'));
    }

    public function editPost(Request $request) {
        $id = $request->id;
        $post = Post::where("id", $id)->first();
        $post["images"] = DB::table("post_image")
        ->leftJoin("images", "images.id", "=", "post_image.image_id")
        ->where("post_image.post_id", "=", $post->id)
        ->get();
        return view('editPost', compact('post'));
    }

    public function handleEditPost(Request $request) {
        toast('Sửa bài viết thành công','success');
        $post = Post::where('id', $request->id)->first();
        $post->update($request->all());

        //TODO: xu ly sua anh
        if ($request->file("images")) {
            foreach ($request->file("images") as $key => $image) {
                $detailImage = $image->storeOnCloudinary();
                $dir = $detailImage->getPath();
                $uploadAddImage = Image::create(
                    [
                        "directory" => $dir,
                        "title" => " ",
                        "content" => " "
                    ]
                );
                PostImage::create(
                    [
                        "post_id" => $post->id,
                        "image_id" => $uploadAddImage->id
                    ]
                );
            }
        }

        return redirect()->route('myPosts');

    }

    public function deletePost(Request $request) {
        $id = $request->id;
        $post = Post::find($id)->delete();
        alert()->success('Thông báo:', 'Xóa bài viết thành công.');
        return redirect()->route('myPosts');
    }

    public function deletePostForum(Request $request) {
        $id = $request->id;
        $post = Post::find($id)->delete();
        alert()->success('Thông báo:', 'Xóa bài viết thành công.');
        return redirect()->route('forum');
    }

    public function deletePostImage($imageId) {
        PostImage::where(["image_id" => $imageId])->first()->delete();
        Image::where(["id" => $imageId])->first()->delete();
        return response()->json(["message" => "Delete successfully", "status" => 200]);
    }
}
