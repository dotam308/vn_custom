<?php

namespace App\Http\Controllers;

use App\Mail\replyMail;
use App\Models\Custom;
use App\Models\CuisineImage;
use App\Models\Image;
use App\Models\feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function handleSendFeedback(Request $request ) {
        $validated = $request->validate([
            'title' => 'required',
            'email' => 'required',
        ]);
        feedback::create([
            'email' => $request->email,
            'title'=> $request->title,
            'content' => $request->content,
            'handled' => "false"
        ]);
        alert()->success('Thông báo','Gửi phản hồi thành công.');
        return redirect()->back();
    }

    public function viewFeedback() {
        $feedbacks = feedback::where('handled', 'false')->paginate(10);
        return view('viewFeedback', compact('feedbacks'));
    }

    public function handleFeedback(Request $request) {
        $feedback = feedback::where('id', $request->id)->first();
        return view('detailFeedback', compact('feedback'));
    }

    public function actionForFeedback(Request $request) {
        if ($request->sendReply) {
            $details = ['body' => $request->content];
            Mail::to($request->email)->send(new replyMail($details));
            alert()->success('Thông báo','Gửi trả lời thành công.');
            $feedback = feedback::where('id', $request->id)->first();
            $feedback['handled'] = 'true';
            $feedback->save();
        }
        else {
            $feedback = feedback::where('id', $request->id)->first();
            $feedback['handled'] = 'true';
            $feedback->save();
        }
        return $this->viewFeedback();
    }
}
