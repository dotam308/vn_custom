<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    public function getMyProfile() {
        $user = User::where('id', '=', session("userId"))->first();
        return view("profile", compact('user'));
    }

    public function changePassword(Request $request) {
        $user = User::where("username", $request->username)->where("password", md5($request->old_password))->first();
        if ($user) {
            $user->password = md5($request->new_password);
            $user->save();
            toast("Chỉnh sửa mật khẩu thành công");
        } else {
            toast("Mật khẩu cũ không chính xác");
        }
        return redirect()->back();
    }

    public function getMyPosts() {
        $posts = Post::where("user_id", session("userId"))->orderBy('id', 'DESC')->paginate(10);
        foreach($posts as $post) {
            $post["images"] = DB::table("post_image")
            ->leftJoin("images", "images.id", "=", "post_image.image_id")
            ->where("post_image.post_id", "=", $post->id)
            ->get();
            $post["username"] = User::where("id", "=", $post->user_id)->get('username')->first();
        }
        return view('myPost', compact('posts'));
    }
}
