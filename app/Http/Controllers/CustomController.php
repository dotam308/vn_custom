<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\Custom;
use App\Models\CustomImage;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomController extends Controller
{
    public function index()
    {
        $customs = Custom::paginate(6);
        return view('custom', compact('customs'));
    }

    public function show($id)
    {
        $custom = Custom::where("id", $id)->first();
        $custom->views++;
        $custom->save();
        $custom["images"] = DB::table("custom_image")
            ->leftJoin("images", "images.id", "=", "custom_image.image_id")
            ->where("custom_image.custom_id", "=", $custom->id)
            ->get();

        $relatedCustoms = Custom::inRandomOrder()->paginate(6);
        return view('detailCustom', compact('custom', 'relatedCustoms'));
    }

    public function deleteCustom(Request $request)
    {
        $custom = Custom::find($request->id)->delete();
        alert()->success('Thông báo:','Xóa phong tục thành công.');
        return redirect()->back();
    }

    public function editCustom(Request $request)
    {
        $custom = Custom::where('id', $request->id)->first();
        $custom["images"] = DB::table("custom_image")
            ->leftJoin("images", "images.id", "=", "custom_image.image_id")
            ->where("custom_image.custom_id", "=", $custom->id)
            ->get();
        return view("editCustom", compact('custom'));
    }

    public function editDetailCustom(Request $request)
    {
        toast('Sửa phong tục thành công','success');
        $custom = Custom::where('id', $request->id)->first();
        $custom->update($request->all());

        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->file("image")) {
            $result = $request->file("image")->storeOnCloudinary();
            $directory = $result->getPath();
            $uploadImage = Image::create(["directory" => $directory]);
            $custom->update(["displayed_image_id" => $uploadImage->id]);
        }

        $imagesOfCustom = CustomImage::where("custom_id", $custom->id)->get();
        if ($request->oldTitles) {
            foreach ($request->oldTitles as $key => $title) {
                $newImage = Image::where(["id" => $imagesOfCustom[$key]->image_id]);
                if (!empty($request->file("oldImages")[$key])) {
                    $detailImage = $request->file("oldImages")[$key]->storeOnCloudinary();
                    $dir = $detailImage->getPath();
                    $newImage->update(["directory" => $dir]);
                }
                $newImage->update(
                    [
                        "title" => $title ?? "",
                        "content" => $request->oldContents[$key] ?? ""
                    ]
                );
            }
        }

        if ($request->file("images")) {
            foreach ($request->file("images") as $key => $image) {
                $detailImage = $image->storeOnCloudinary();
                $dir = $detailImage->getPath();
                $uploadAddImage = Image::create(
                    [
                        "directory" => $dir,
                        "title" => $request->titles[$key] ?? "",
                        "content" => $request->contents[$key] ?? ""
                    ]
                );
                CustomImage::create(
                    [
                        "custom_id" => $custom->id,
                        "image_id" => $uploadAddImage->id
                    ]
                );
            }
        }
        // return redirect()->back();
        return redirect()->route("manageCustoms");
    }

    public function addCustom()
    {
        return view("addCustom");
    }

    public function addCustomPost(Request $request)
    {
        toast('Thêm phong tục thành công','success');
        $custom = Custom::create($request->all());
        if ($request->yes && $request->mail == "yes") Helpers::sendMailToSubscriber($custom->id, $custom->title, "custom");

        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->file("image")) {
            $result = $request->file("image")->storeOnCloudinary();
            $directory = $result->getPath();
            $uploadImage = Image::create(["directory" => $directory]);
            $custom->update(["displayed_image_id" => $uploadImage->id]);
        }
        if ($request->file("images")) {
            foreach ($request->file("images") as $key => $image) {
                $detailImage = $image->storeOnCloudinary();
                $dir = $detailImage->getPath();
                $uploadAddImage = Image::create(
                    [
                        "directory" => $dir,
                        "title" => $request->titles[$key],
                        "content" => $request->contents[$key]
                    ]
                );
                CustomImage::create(
                    [
                        "custom_id" => $custom->id,
                        "image_id" => $uploadAddImage->id
                    ]
                );
            }
        }
        if ($request->addOne) {
            return redirect()->route("manageCustoms");
        }
        return redirect()->back();
    }

    public function deleteCustomImage($imageId)
    {
        $customImage = CustomImage::where(["image_id" => $imageId])->first();
        if ($customImage) $customImage->delete();
        $custom = Custom::where(["displayed_image_id" => $imageId])->first();
        if ($custom) $custom->update(["displayed_image_id" => NULL]);
        $image = Image::where(["id" => $imageId])->first();
        if ($image) $image->delete();

        return response()->json(["message" => "Delete successfully", "status" => 200]);
    }

    public function broadcastCustom($id) {
        Helpers::sendMailToSubscriber($id, null, "custom");
        return redirect()->route("manageCustoms");
    }
}
