<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (session("role") && session("role") == "admin") {
            return $next($request);
        }
        alert()->warning("Cảnh báo", "Bạn không có quyền truy cập trang này");
        return redirect()->route("home");
    }
}
