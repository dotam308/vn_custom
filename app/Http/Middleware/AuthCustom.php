<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!session("role")) {
            alert()->warning("Cảnh báo", "Vui lòng đăng nhập trước");
            return redirect()->route("home");
        }
        return $next($request);
    }
}
