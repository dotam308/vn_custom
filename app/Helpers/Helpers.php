<?php

namespace App\Helpers;

use App\Mail\notification;
use App\Models\Cuisine;
use App\Models\Custom;
use App\Models\Subscriber;
use Illuminate\Support\Facades\Mail;

class Helpers
{
    /**
     * @param $valueSearch
     * @return string|string[]|null
     */
    public static function sendMailToSubscriber($id, $content = null, $type = null)
    {
        if ($type == "custom") {
            $content = $content ?? Custom::where("id", $id)->first()->title;
            $link = env("APP_URL_FULL") . "detail-custom/$id";
        } else if ($type == "cuisine") {
            $content = $content ?? Cuisine::where("id", $id)->first()->title;
            $link = env("APP_URL_FULL") . "detail-cuisine/$id";
        }
        $details = [
            'body' => "Vào ThaiTour đọc ngay bài viết mới thôi: {{ $content }}",
            "link" => $link
        ];      //gui mail bao bai viet moi
        $receiver = Subscriber::all();
        foreach ($receiver as $member) {
            Mail::to($member)->send(new notification($details));
        }
    }
}
