<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomImage extends Model
{
    use HasFactory;
    protected $table = "custom_image";
    protected $fillable = ['id', 'custom_id', 'image_id'];
}
