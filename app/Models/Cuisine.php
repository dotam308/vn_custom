<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'content',
        'title',
        'short_description',
        'displayed_image_id',
        'views'
    ];

    // protected $searchable = ['title', 'content', 'short_description'];
    
    public function displayedImage()
    {
        return $this->belongsTo(Image::class);
    }
}
