<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'src',
        'image_id',
        'image_url',
        "short_description",
        "content"
    ];

    public function image() {
        return $this->belongsTo(Image::class);
    }
}
