<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuisineImage extends Model
{
    use HasFactory;
    protected $table = "cuisine_image";
    protected $fillable = ['id', 'cuisine_id', 'image_id'];
}
