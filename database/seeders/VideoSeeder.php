<?php

namespace Database\Seeders;

use App\Models\Video;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Video::create([
            "src" => "https://www.youtube.com/embed/p1UvGFvGpts",
            "image_url" => "https://res.cloudinary.com/dbiexlh94/image/upload/v1639753663/maxresdefault_dzvzdv.jpg",
            "short_description" => "Thung lũng mận",
            "content" => "Nhắc tới Tây Bắc, người ta sẽ nghĩ ngay tới Mộc Châu. Nhắc đến Mộc Châu, du khách sẽ  nhớ ngay đến vùng đất của những mùa hoa cải trắng. Đương nhiên, cũng không thể không nhắc đến rừng mận Mộc Châu – chốn thiên đường giữa những lưng chừng mây. Một trong những điểm đến nổi tiếng của rừng mận Mộc Châu chính là thung lũng mận Nà Ka."
        ]);
        Video::create([
            "src" => "https://www.youtube.com/embed/-1XRms-iPbY",
            "image_url" => "https://dulichkhampha24.com/wp-content/uploads/2019/12/thac-dai-yem-moc-chau.jpg",
            "short_description" => "Thác dải yếm",
            "content" => "Cao nguyên Mộc Châu được biết đến như là một cao nguyên rộng lớn và xinh đẹp nhất vùng phía Tây Bắc. Tại đây nổi bật lên với cảnh quan núi rừng kỳ vĩ, những con đường uốn éo ôm sát sườn núi, những lớp sương mù dày đặc khiến cảnh vật nơi đây đẹp lạ, mê mẩn lòng người. Không chỉ vậy, ở ngay trong lòng cao nguyên Mộc Châu còn xuất hiện một “báu vật” làm nao nức các khách du lịch khi đến đây, đó chính là Thác Dải Yếm."
        ]);
        Video::create([
            "src" => "https://www.youtube.com/embed/fmQnuLL31LQ",
            "image_url" => "https://bazantravel.com/cdn/medias/uploads/50/50668-cau-kinh-5d-viet-nam-2.jpg",
            "short_description" => "Cầu kính tình yêu",
            "content" => "Bên cạnh dòng thác Dải Yếm mềm mại tung bọt trắng, cầu kính tình yêu buông sắc vàng đỏ rực rỡ nổi bật trên nền núi rừng xanh thẳm. Chiếc cầu vững vàng kết nối hai triền núi lại với nhau, hình thành nên vị trí ngắm nhìn toàn cảnh xung quanh với tầm nhìn 360 độ. Cầu kính có có độ cao 22m so với dòng suối phía dưới, được kết từ 10 cụm trái tim, dưới 10 trái tim có 10 cánh sen tạo nên hình bông sen. Đây là cây cầu kính đầu tiên của Việt Nam có chiều dài 80m, rộng 2m, được làm từ vật liệu chính là sắt thép và kính chống đạn, đảm bảo độ bền và an toàn cho du khách tham quan."

        ]);
        Video::create([
            "src" => "https://www.youtube.com/embed/H4fU2OP3vzo",
            "image_url" => "https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2020/03/tuyet-tinh-coc-ninh-binh-1.jpg",
            "short_description" => "Tuyệt tình cốc",
            "content" => "Tuyệt Tình Cốc Ninh Bình là một trong những danh lam thắng cảnh nổi tiếng tại vùng đất Cố Đô. Nơi đây không chỉ khắc ghi vào lòng du khách thập phương bởi vẻ đẹp hoang sơ, hữu tình mà còn có nhiều hoạt động cực kỳ thú vị đang đợi chờ bước chân khám phá!"
        ]);
        Video::create([
            "src" => "https://www.youtube.com/embed/2yCm4bFDAiA",
            "image_url" => "https://media-cdn.laodong.vn/storage/newsportal/Uploaded/hoangvanmanh/2014_01_17/PL4_DRFN.jpg",
            "short_description" => "Huyền thoại sao va",
            "content" => "Một bài ca mang nhiều âm hưởng miền núi, dân tộc, kết hợp cùng hình ảnh, âm thanh vùng cao giúp ta có thêm nhiều cảm xúc về nơi đây."
        ]);
        Video::create([
            "src" => "https://www.youtube.com/embed/izxSZM_YZJw",
            "image_url" => "https://res.cloudinary.com/dbiexlh94/image/upload/v1639754465/90_kelng9.png",
            "short_description" => "Phong tục đón Tết của người Thái Quỳ Hợp",
            "content" => "Trong đêm giao thừa, người lớn ngồi quây quần bên bếp lửa để chào đón những giây phút quan trọng, sự chuyển giao của năm cũ sang năm mới và chú ý nghe ngóng xem trong đêm ấy, con gì kêu trước để định đoán thời tiết. Nếu như con nai rừng kêu trước thì sang năm mới sẽ làm ăn khó. Nếu như con mèo mà kêu trước thì sang năm mới sẽ loạn cọp (hổ)… Còn các chàng trai, cô gái tiếng trống chiêng, tiếng hát giao duyên vẫn vang lên đều đặn trong đêm giao thừa. Người Thái có tục giữ lửa bếp trong suốt đêm giao thừa. Đồng bào quan niệm rằng nếu bếp lửa mà tắt thì năm mới sẽ gặp phải nhiều điều không may mắn."
        ]);
        Video::create([
            "src" => "https://www.youtube.com/embed/gj8LUY7XsKE",
            "image_url" => "https://res.cloudinary.com/dbiexlh94/image/upload/v1639754587/19_apgooj.png",
            "short_description" => "Giá trị truyền thống văn hoá dân tộc Thái",
            "content" => ""
        ]);
        Video::create([
            "src" => "https://www.youtube.com/embed/0O9tO7FRqLY",
            "image_url" => "https://i.ytimg.com/vi/0O9tO7FRqLY/maxresdefault.jpg",
            "short_description" => "Nguồn gốc người Thái Việt Nam",
            "content" => "Người Thái với tên tự gọi là Tãy/Tay/Tày/Thay tùy thuộc vào cách phát âm của từng khu vực. Các nhóm, ngành lớn của người Thái tại Việt Nam bao gồm: Tay Đón (Thái Trắng), Tay Đăm (Thái Đen), Tay Đèng (Thái Đỏ) và Tay Dọ (Thái Yo) cùng một số khác nhỏ hơn. Họ đã có mặt ở miền Tây Bắc Việt Nam trên 1200 năm, là hậu duệ những người Thái đã di cư từ vùng đất thuộc tỉnh Vân Nam, Trung Quốc bây giờ..."
        ]);

    }
}
