<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string("username");
            $table->string("password");
            $table->string("role");
            $table->timestamps();
        });
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->string("directory");
            $table->timestamps();
        });
        Schema::create('customs', function (Blueprint $table) {
            $table->id();
            $table->longText("content");
            $table->string("title");
            $table->longText("short_description")->nullable();
            $table->unsignedBigInteger("displayed_image_id")->nullable();
            $table->foreign("displayed_image_id")->references("id")->on("images")->onDelete("cascade");
            $table->timestamps();
        });
        Schema::create('custom_image', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("custom_id")->nullable();
            $table->unsignedBigInteger("image_id")->nullable();
            $table->foreign("custom_id")->references("id")->on("customs")->onDelete("cascade");
            $table->foreign("image_id")->references("id")->on("images")->onDelete("cascade");
            $table->timestamps();
        });
        Schema::create('cuisines', function (Blueprint $table) {
            $table->id();
            $table->longText("content");
            $table->string("title");
            $table->longText("short_description")->nullable();
            $table->unsignedBigInteger("displayed_image_id")->nullable();
            $table->foreign("displayed_image_id")->references("id")->on("images")->onDelete("cascade");
            $table->timestamps();
        });
        Schema::create('cuisine_image', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("cuisine_id")->nullable();
            $table->unsignedBigInteger("image_id")->nullable();
            $table->foreign("cuisine_id")->references("id")->on("cuisines")->onDelete("cascade");
            $table->foreign("image_id")->references("id")->on("images")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('images');
        Schema::dropIfExists('customs');
        Schema::dropIfExists('custom_image');
        Schema::dropIfExists('cuisines');
        Schema::dropIfExists('cuisine_image');
    }
}
