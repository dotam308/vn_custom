<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddViewsToCustomAndCuisine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customs', function (Blueprint $table) {
            $table->unsignedBigInteger("views")->default(0);
        });
        Schema::table('cuisines', function (Blueprint $table) {
            $table->unsignedBigInteger("views")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customs', function (Blueprint $table) {
            $table->dropColumn("views");
        });
        Schema::table('cuisines', function (Blueprint $table) {
            $table->dropColumn("views");
        });
    }
}
